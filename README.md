# Tray - a Preferences replacement for Ohos

If you have read the documentation of the [`Preferences`](https://developer.harmonyos.com/cn/docs/documentation/doc-references/preferences-0000001054518652) you might have seen one of these warnings:

>Note: This class does not support use across multiple processes.

 multiprocess support  it never worked relieable


Tray is this mentioned _explicit cross-process data management approach_ powered by a [`Data Ability`](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ability-data-concept-0000000000043058). Tray also provides an advanced API which makes it super easy to access and maintain your data with upgrade and migrate mechanisms. Welcome to Preferences 2.0 aka Tray.

## Features

- **works multiprocess**
- stores simple data types as key value pairs
- automatically saves metadata for each entry (created, last updated, ...)
- manage your Preferences in modules [TrayPreference]
- Delete single modules, all modules, or [all modules except some very important ones]
- update and migrate your data from one app version to next one with versioned Preferences and a [`onUpgrade()`]  method
- Migrate your current data stored in the SharedPreferences to Tray with [`SharedPreferencesImport`]
- **tray is 100% unit tested!**
- 0 lint warnings/errors

## Preview
![](img/tray.gif)


## Usage

Simple tutorial how to use Tray in your project instead of the Preferences

### Save and read preferences

```java
// create a preference accessor. This is for global app preferences.
final AppPreferences appPreferences = new AppPreferences(getContext()); // this Preference comes for free from the library
// save a key value pair
appPreferences.put("key", "lorem ipsum");

// read the value for your key. the second parameter is a fallback (optional otherwise throws)
final String value = appPreferences.getString("key", "default");
Log.v(TAG, "value: " + value); // value: lorem ipsum

// read a key that isn't saved. returns the default (or throws without default)
final String defaultValue = appPreferences.getString("key2", "default");
Log.v(TAG, "value: " + defaultValue); // value: default
```

No `Editor`, no `commit()` or `apply()` :wink:

### Create your own preference module

It's recommended to bundle preferences in groups, so called modules instead of putting everything in one global module. If you were using `SharedPreferences` before, you might have used different files to group your preferences. Extending the `TrayModulePreferences` and put all Keys inside this class is a recommended way to keep your code clean.

```java
// create a preference accessor for a module
public class MyModulePreference extends TrayPreferences {

    public static String KEY_IS_FIRST_LAUNCH = "first_launch";

    public MyModulePreference(final Context context) {
        super(context, "myModule", 1);
    }
}
```

```java
// accessing the preferences for my own module
final MyModulePreference myModulePreference = new MyModulePreference(getContext());
myModulePreference.put(MyModulePreference.KEY_IS_FIRST_LAUNCH, false);
```

See the sample project for more

Like the Ohos [`RdbOpenCallback`](https://developer.harmonyos.com/cn/docs/documentation/doc-references/rdbopencallback-0000001054120145) a `TrayPreference` lets you implement methods to handle versioning.

```java
public class MyModulePreference extends TrayPreferences {

    public MyModulePreference(final Context context) {
        super(context, "myModule", 1);
    }

    @Override
    protected void onCreate(final int initialVersion) {
        super.onCreate(initialVersion);
    }

    @Override
    protected void onUpgrade(final int oldVersion, final int newVersion) {
        super.onUpgrade(oldVersion, newVersion);
    }

    @Override
    protected void onDowngrade(final int oldVersion, final int newVersion) {
        super.onDowngrade(oldVersion, newVersion);
    }
}

```

`// TOOD add clear sample`

### Migrate from Preferences to Tray

To migrate values from Preferences you have to create you own preference module. This module will be now store all of your Preferences values.

```java
public class ImportPreferences extends TrayPreferences {

    // The SharedPreferences name
    private static final String SHARED_PREFERENCES_FILE_NAME = "PREF_NAME";
    
    // The key inside the SHARED_PREFERENCES_NAME
    private static final String KEY_FIRST_LAUNCH = "KEY_FIRST_LAUNCH";
    
    // The new key for this module
    private static final String KEY_FIRST_LAUNCH_TRAY = "KEY_FIRST_LAUNCH_TRAY";
    
    public ImportPreferences(@NonNull Context context) {
        super(context, "myImportModule", 1);
    }    
    
    // Called only once when the module was created
    @Override
    protected void onCreate(int initialVersion) {
        super.onCreate(initialVersion);
            
        // Create a SharedPreferencesImport object
        SharedPreferencesImport importPref = 
            new SharedPreferencesImport(getContext(), 
                SHARED_PREFERENCES_FILE_NAME, KEY_FIRST_LAUNCH, KEY_FIRST_LAUNCH_TRAY);
            
        // Finally migrate it
        migrate(importPref);
    }
}
```

## Getting Started

##### Add Tray to your project

    1.Copy the Tray/library folder to the project directory
    
    2.Modify settings.gradle under the project and add the dependency on this module as follows:
    
    include ':entry', ':library'
    
    3. Introduce the dependency of the module under the project. Taking the entry module as an example, you need to modify the build.gradle file under the entry module to add the dependency:
    
    compile project(path: ':library') or implementation project(':library')
    
    Solution 2: local use of har package integration
    1. Compile this project, copy the har package generated in the build directory of the Tray/library folder to the project lib folder:
    directory：\Tray\library\build\outputs\har\debug\library-debug.har
    
    2. Add the following code in the entry's gradle
    implementation fileTree(dir:'libs', include: ['*.jar','*.har'])
    
    Solution 3:  remote maven repo integration
    Add the following code in the entry's gradle
    implementation 'com.gitee.baijuncheng-open-source:tray:1.0.1'


More on the `Data Ability` configuration can be found in the Project.

## Project state

Tray is currently in active development by [grandcentrix]. We decided to go open source after reaching 100% test coverage.
[grandcentrix] uses Tray in production in two apps without problems.

You can follow the development in the [`develop`] branch.

## Testcoverage 100%

Tray has 100% test coverage and we'll try to keep it at that level for stable releases.

## Data Ability is overkill

At first, it was the simplest way to use IDL with  to solve the multiprocess problem. Using the `Data Ability` with a database turned out to be very handy when it comes to save metadata. We thought about replacing the database with the real `Preferences` to boost the performance (the Preferences do not access the disk for every read/write action which causes the multiprocess problem btw) but the metadata seemed to be more valuable to us. see [more informations]

If you have found a better solution implement the library/src/main/java/net/grandcentrix/tray/core/TrayStorage.java and contribute to this project! We would appreciate it.

That said, yes the performance isn't as good as the SharedPreferences. But the performance is good enough to save/access single key value pairs synchron. If you want to save more you should think about a simple database.

## Missing Features

Tray is ready to use without showblockers! But here are some nice to have features for the future:
- Reactive wrapper to observe values 
- no support to save `Set<String>`. Is someone using this?
- more metadata fields: (i.e. app version code/name)

## Roadmap

- performance tests
- memory cache for based on contentobservers
- prevent app crashes due to database errors
- rx wrapper for changes
- save additional data types (`Set<String>`, `byte[]`)

## Versions

##### Version 0.11.1 `07.02.17`

- preference key cannot be empty #84
- `clearBut(TrayPreference)` -> `clearBut(AbstractTrayPreference)` #89

##### Version 0.11.0 `07.09.16`
- all accessor methods return `boolean` indicating the success of i.e. `put`, `remove`. They will never again throw an error. #69
- new `contains()` method #74

##### Version 0.10.0 `31.05.16`
- All features and changes of the 1.0.0-rc preview builds
- #65 Fix deletion of non string migrated shared preferences.

>##### Version 1.0.0 preview - postponed until the memory cache is ready

>###### 1.0.0-rc3 `05.11.15`
- the sample project includes now a way to test the multi process support compared to the `SharedPreferences`
- removed unnecessary write operation for every version check #54

>###### 1.0.0-rc2 `24.09.15`
- added logging for all data changing methods. Enable via `adb shell setprop log.tag.Tray VERBOSE`

>###### 1.0.0-rc1 `21.09.15`
- Auto Backup feature support**
    - split up database for *user* and *device* specific data (device specific data can now be excluded from the auto backup)
    - `TrayPreferences` has now an optional 3. constructor parameter `TrayStorage.Type`, `USER` or `DEVICE` indicating the internal database . Default is `USER`
- **New methods and changes**
    - `PreferenceAccessor#wipe()` clears the preference data and it's internal data (version)
    - `TrayPreferences#annexModule(String name)` imports a module by name and wipes it afterwards. This allows renaming of preferences without losing data
    - `AbstractTrayPreference#annex(ModularizedStorage<TrayItem>)` allows a storage to import another storage, wipes the imported afterwards
    - `Preference` `#onCreate(...)` and `#onUpgrade(...)` aren't abstract anymore because they don't require an implementation
- **Deprecations** (will be removed soon)
    - `TrayAppPreferences` is now deprecated. Use `AppPreferences` instead (renaming)
    - `TrayModulePreferences` is now deprecated. Use `TrayPreferences` instead to extend from for your own Preferences
- **Internal structure**
    - new package structure. merged packages `accessor`, `migration` and `storage` into `core`
    - package `provider` contains a `TrayStorage` implementation with a `Data Ability`. Is easy exchangeable with another `TrayStorage` implementation
    - `ModularizedTrayPreference` is now called `AbstractTrayPreference`
    - `ModularizedStorage` was renamed to `TrayStorage`


##### Version 0.9.2 `02.06.15`
- `getContext()` is working in `TrayModulePreference#onCreate`

##### Version 0.9.1 `18.05.15`
- saving `null` with `mPref.put(KEY, null)` works now
- access to preference with throwing methods instead of default value (throws ItemNotFoundException). Example: `mPref.getString(KEY);` instead of `mPref.getString(KEY, "defaultValue");`
- WrongTypeException when accessing a preference with a different type and the data isn't parsable. Float (`10.1f`) -> String works, String (`"10.1"`) -> Float works, String (`"test"`) -> Float throws!
- javadoc in now included in aar

##### Version 0.9 `27.04.15`
- initial public release

##### Version 0.2 - 0.8
- Refactoring
- 100% Testing
- Bugfixing

##### Version 0.1 `17.09.14`
- first working prototype


## Contributors

- Pascal Welsch - https://github.com/passsy
- Jannis Veerkamp - https://github.com/jannisveerkamp

# License

```
Copyright 2015 grandcentrix GmbH

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
