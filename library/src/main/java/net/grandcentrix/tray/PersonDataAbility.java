/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.grandcentrix.tray;

import net.grandcentrix.tray.annotation.NonNull;
import net.grandcentrix.tray.attach.DatabaseUtils;
import net.grandcentrix.tray.attach.UriMatcher;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.SqliteHelper;
import net.grandcentrix.tray.provider.TrayContract;
import net.grandcentrix.tray.provider.TrayDBHelper;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.data.rdb.*;
import ohos.data.resultset.CombinedResultSet;
import ohos.data.resultset.ResultSet;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import ohos.utils.PacMap;

import java.io.FileDescriptor;
import java.util.Date;

public class PersonDataAbility extends Ability {
    public static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "Demo");
    TrayDBHelper mDeviceDbHelper;

    TrayDBHelper mUserDbHelper;

    private static final int SINGLE_PREFERENCE = 10;

    private static final int MODULE_PREFERENCE = 20;

    private static final int ALL_PREFERENCE = 30;

    private static final int INTERNAL_SINGLE_PREFERENCE = 110;

    private static final int INTERNAL_MODULE_PREFERENCE = 120;

    private static final int INTERNAL_ALL_PREFERENCE = 130;

    private static UriMatcher sURIMatcher;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        HiLog.info(LABEL_LOG, "TrayDataAbility onStart");
        mUserDbHelper = new TrayDBHelper(getContext(), true);
        mDeviceDbHelper = new TrayDBHelper(getContext(), false);
        setAuthority("net.grandcentrix.tray.PersonDataAbility");
    }

    @Override
    public ResultSet query(Uri uri, String[] columns, DataAbilityPredicates predicates) {
        String selection = predicates.getWhereClause();
        String[] selectionArgs = new String[predicates.getWhereArgs().size()];
        predicates.getWhereArgs().toArray(selectionArgs);

        uri = UriMatcher.convertUri(uri);
        final int match = sURIMatcher.match(uri);
        switch (match) {
            case SINGLE_PREFERENCE:
            case INTERNAL_SINGLE_PREFERENCE:
                selection = SqliteHelper.extendSelection(selection, TrayContract.Preferences.Columns.KEY + " = " +
                        DatabaseUtils.sqlEscapeString(uri.getDecodedPathList().get(2)));
                // no break
            case MODULE_PREFERENCE:
            case INTERNAL_MODULE_PREFERENCE:
                if (match == SINGLE_PREFERENCE
                        || match == INTERNAL_SINGLE_PREFERENCE) {
//                    builder.appendWhere(" AND ");
//                    selection = SqliteHelper.extendSelection(selection," AND ");
                }

                selection = SqliteHelper.extendSelection(selection, TrayContract.Preferences.Columns.MODULE + " = " +
                        DatabaseUtils.sqlEscapeString(uri.getDecodedPathList().get(1)));

                // no break
            case ALL_PREFERENCE:
            case INTERNAL_ALL_PREFERENCE:
//                builder.setTables(getTable(uri));
                break;
            default:
                throw new IllegalArgumentException("Query is not supported for Uri: " + uri);
        }
        final ResultSet resultSet;
        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(getTable(uri), selection, selectionArgs);
        final String backup = uri.getFirstQueryParamByKey("backup");
        if (backup == null) {
            // backup not set, query both dbs
            ResultSet userResultSet1 = mUserDbHelper.getStore().query(rawRdbPredicates, columns);

            ResultSet deviceResultSet2 = mDeviceDbHelper.getStore().query(rawRdbPredicates, columns);
            resultSet = new CombinedResultSet(new ResultSet[]{userResultSet1, deviceResultSet2});
        } else {
            // Query
            resultSet = getReadableDatabase(uri).query(rawRdbPredicates, columns);

        }
        /**query don't need  notify. Prevent the continuing query  after receiving the monitoring,may result in a loop**/
        //if (resultSet != null) {
//            DataAbilityHelper.creator(this, UriMatcher.revertUri(uri)).notifyChange(UriMatcher.revertUri(uri));
        //}
        return resultSet;
    }

    @Override
    public int insert(Uri uri, ValuesBucket values) {
        HiLog.info(LABEL_LOG, "TrayDataAbility insert");
        Date date = new Date();
        uri = UriMatcher.convertUri(uri);
        final int match = sURIMatcher.match(uri);
        switch (match) {
            case SINGLE_PREFERENCE:
            case INTERNAL_SINGLE_PREFERENCE:
                // Add created and updated dates
                long currentTime = date.getTime();
                values.putLong(TrayContract.Preferences.Columns.CREATED, currentTime);
                values.putLong(TrayContract.Preferences.Columns.UPDATED, currentTime);
                values.putString(TrayContract.Preferences.Columns.MODULE, uri.getDecodedPathList().get(1));
                values.putString(TrayContract.Preferences.Columns.KEY, uri.getDecodedPathList().get(2));
                break;

            default:
                throw new IllegalArgumentException("Insert is not supported for Uri: " + uri);
        }

        final String prefSelection =
                TrayContract.Preferences.Columns.MODULE + " = ?"
                        + "AND " + TrayContract.Preferences.Columns.KEY + " = ?";
        final String[] prefSelectionArgs = {
                values.getString(TrayContract.Preferences.Columns.MODULE),
                values.getString(TrayContract.Preferences.Columns.KEY)
        };

        final String[] excludeForUpdate = {TrayContract.Preferences.Columns.CREATED};

        final int status = insertOrUpdate(getReadableDatabase(uri), getTable(uri),
                prefSelection, prefSelectionArgs, values, excludeForUpdate);

        if (status >= 0) {
            DataAbilityHelper.creator(getContext()).notifyChange(UriMatcher.revertUri(uri));
            return status;

        } else if (status == -1) {
            //throw new SQLiteException("An error occurred while saving preference.");
            TrayLog.w("Couldn't update or insert data. Uri: " + uri);
        } else {
            TrayLog.w("unknown SQLite error");
        }
        return status;
    }

    @Override
    public int delete(Uri uri, DataAbilityPredicates predicates) {
        uri = UriMatcher.convertUri(uri);
        final int match = sURIMatcher.match(uri);
        RawRdbPredicates rawRdbPredicates = null;
        String selection = predicates.getWhereClause();
        String[] selectionArgs = new String[predicates.getWhereArgs().size()];
        predicates.getWhereArgs().toArray(selectionArgs);
        switch (match) {
            case SINGLE_PREFERENCE:
            case INTERNAL_SINGLE_PREFERENCE:

                selection = SqliteHelper.extendSelection(selection,
                        TrayContract.Preferences.Columns.KEY + " = ?");

                selectionArgs = SqliteHelper.extendSelectionArgs(selectionArgs,
                        new String[]{uri.getDecodedPathList().get(2)});

                // no break
            case MODULE_PREFERENCE:
            case INTERNAL_MODULE_PREFERENCE:

                selection = SqliteHelper.extendSelection(selection,
                        TrayContract.Preferences.Columns.MODULE + " = ?");

                selectionArgs = SqliteHelper.extendSelectionArgs(selectionArgs,
                        new String[]{uri.getDecodedPathList().get(1)});

                rawRdbPredicates = new RawRdbPredicates(getTable(uri), selection, selectionArgs);

                // no break
            case ALL_PREFERENCE:
            case INTERNAL_ALL_PREFERENCE:
                break;
            default:
                throw new IllegalArgumentException("Delete is not supported for Uri: " + uri);
        }

        final int rows;
        final String backup = uri.getFirstQueryParamByKey("backup");
        if (backup == null) {
            int device = mDeviceDbHelper.getStore()
                    .delete(rawRdbPredicates);
            int user = mUserDbHelper.getStore()
                    .delete(rawRdbPredicates);
            rows = device + user;
        } else {
            rows = getReadableDatabase(uri)
                    .delete(rawRdbPredicates);
        }

        // Don't force an UI refresh if nothing has changed
        if (rows > 0) {
            DataAbilityHelper.creator(getContext()).notifyChange(UriMatcher.revertUri(uri));
        }

        return rows;
    }

    @Override
    public int update(Uri uri, ValuesBucket value, DataAbilityPredicates predicates) {
        return 0;
    }

    @Override
    public FileDescriptor openFile(Uri uri, String mode) {
        return null;
    }

    @Override
    public String[] getFileTypes(Uri uri, String mimeTypeFilter) {
        return new String[0];
    }

    @Override
    public PacMap call(String method, String arg, PacMap extras) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }


    public RdbStore getReadableDatabase(final Uri uri) {
        if (shouldBackup(uri)) {
            return mUserDbHelper.getStore();
        } else {
            return mDeviceDbHelper.getStore();
        }
    }

    /**
     * checks the uri for the backup param. default is that
     *
     * @param uri contentUri
     * @return default true or false for {@code /the/uri&backup=false}
     */
    boolean shouldBackup(@NonNull final Uri uri) {
        final String backup = uri.getFirstQueryParamByKey("backup");
        return !"false".equals(backup);
    }

    /**
     * @param uri localtion of the data
     * @return correct sqlite table for the given uri
     */
    public String getTable(final Uri uri) {
        if (uri == null) {
            return null;
        }
        final int match = sURIMatcher.match(uri);
        switch (match) {
            case SINGLE_PREFERENCE:
            case MODULE_PREFERENCE:
            case ALL_PREFERENCE:
            default:
                return TrayDBHelper.TABLE_NAME;

            case INTERNAL_SINGLE_PREFERENCE:
            case INTERNAL_MODULE_PREFERENCE:
            case INTERNAL_ALL_PREFERENCE:
                return TrayDBHelper.INTERNAL_TABLE_NAME;
        }
    }

    public int insertOrUpdate(final RdbStore writableDatabase, final String table,
                              final String prefSelection, final String[] prefSelectionArgs,
                              final ValuesBucket values, final String[] excludeForUpdate) {
        return SqliteHelper
                .insertOrUpdate(writableDatabase, table, prefSelection, prefSelectionArgs, values,
                        excludeForUpdate);
    }


    static void setAuthority(final String authority) {
        sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        sURIMatcher.addURI(authority,
                TrayContract.Preferences.BASE_PATH,
                ALL_PREFERENCE);
        // BASE/module
        sURIMatcher.addURI(authority,
                TrayContract.Preferences.BASE_PATH + "/*",
                MODULE_PREFERENCE);

        // BASE/module/key
        sURIMatcher.addURI(authority,
                TrayContract.Preferences.BASE_PATH + "/*/*",
                SINGLE_PREFERENCE);

        sURIMatcher.addURI(authority,
                TrayContract.InternalPreferences.BASE_PATH,
                INTERNAL_ALL_PREFERENCE);

        // INTERNAL_BASE/module
        sURIMatcher.addURI(authority,
                TrayContract.InternalPreferences.BASE_PATH + "/*",
                INTERNAL_MODULE_PREFERENCE);

        // INTERNAL_BASE/module/key
        sURIMatcher.addURI(authority,
                TrayContract.InternalPreferences.BASE_PATH + "/*/*",
                INTERNAL_SINGLE_PREFERENCE);
    }
}