/*
 * Copyright (C) 2015 grandcentrix GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.grandcentrix.tray.provider;


import net.grandcentrix.tray.annotation.NonNull;
import net.grandcentrix.tray.annotation.Nullable;
import net.grandcentrix.tray.core.AbstractTrayPreference;
import net.grandcentrix.tray.core.TrayException;
import net.grandcentrix.tray.core.TrayItem;
import net.grandcentrix.tray.PersonDataAbility;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.app.Context;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.sysappcomponents.calendar.column.BaseColumns;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static net.grandcentrix.tray.attach.Log.LABEL;

/**
 * Helper for accessing the
 * <p>
 * Created by pascalwelsch on 11/20/14.
 */
public class TrayProviderHelper {

    private final Context mContext;

    private final TrayUri mTrayUri;
    private static   final   String [] columns = new String[]{TrayContract.Preferences.Columns.MODULE,
                                                            TrayContract.Preferences.Columns.KEY,
                                                            TrayContract.Preferences.Columns.MIGRATED_KEY,
                                                            TrayContract.Preferences.Columns.VALUE,
                                                            TrayContract.Preferences.Columns.CREATED,
                                                            TrayContract.Preferences.Columns.UPDATED};

    public TrayProviderHelper(@NonNull final Context context) {
        mContext = context;
        mTrayUri = new TrayUri(context);
    }

    /**
     * clears <b>all</b> Preferences saved. Module independent. Erases all preference data
     *
     * @return true when successful
     */
    public boolean clear() {
        try {
            DataAbilityHelper databaseHelper = DataAbilityHelper.creator(mContext);
            DataAbilityPredicates predicates = new DataAbilityPredicates();
            predicates.greaterThan(BaseColumns.ID,-1);
            // result can be 0 for an empty module, don't check if rows > 0
//            mContext.getContentResolver().delete(mTrayUri.get(), null, null);
            databaseHelper.delete(mTrayUri.get(), predicates);
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * clears <b>all</b>  but the modules stated.
     *
     * @param modules modules excluded when deleting preferences
     * @return true when successful, false otherwise. true doesn't indicate that something got
     * cleared, it just means no error occurred
     */
    public boolean clearBut(AbstractTrayPreference... modules) {
        DataAbilityHelper databaseHelper = DataAbilityHelper.creator(mContext);
        DataAbilityPredicates predicates = new DataAbilityPredicates();
        for (final AbstractTrayPreference module : modules) {
            if (module == null) {
                continue;
            }
            String moduleName = module.getName();
//            selection = SqliteHelper
//                    .extendSelection(selection, TrayContract.Preferences.Columns.MODULE + " != ?");
//            selectionArgs = SqliteHelper
//                    .extendSelectionArgs(selectionArgs, new String[]{moduleName});
            predicates.notEqualTo(TrayContract.Preferences.Columns.MODULE,moduleName);
        }

        try {
            // result can be 0 for an empty module, don't check if rows > 0
            databaseHelper.delete(mTrayUri.get(),predicates);
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Builds a list of all Preferences saved.
     *
     * @return all Preferences as list.
     */
    @NonNull
    public List<TrayItem> getAll() {
        return queryProviderSafe(mTrayUri.get());
    }


    /**
     * saves the value into the database.
     *
     * @param module module name
     * @param key    key for mapping
     * @param value  data to save
     * @return true when successfully written
     */
    public boolean persist(@NonNull final String module, @NonNull final String key,
                           @NonNull final String value) {
        return persist(module, key, null, value);
    }

    /**
     * saves the value into the database combined with a previousKey.
     *
     * @param module      module name
     * @param key         key for mapping
     * @param previousKey key used before migration
     * @param value       data to save
     * @return true when successfully written
     */
    public boolean persist(@NonNull final String module, @NonNull final String key,
                           @Nullable final String previousKey, @Nullable final String value) {
        final Uri uri = mTrayUri.builder()
                .setModule(module)
                .setKey(key)
                .build();
        return persist(uri, value, previousKey);
    }

    public boolean persist(@NonNull final Uri uri, @Nullable String value) {
        return persist(uri, value, null);
    }

    public boolean persist(@NonNull final Uri uri, @Nullable String value,
                           @Nullable final String previousKey) {
        ValuesBucket values = new ValuesBucket();
        values.putString(TrayContract.Preferences.Columns.VALUE, value);
        values.putString(TrayContract.Preferences.Columns.MIGRATED_KEY, previousKey);

        DataAbilityHelper databaseHelper = DataAbilityHelper.creator(mContext);
//        DataAbilityPredicates predicates = new DataAbilityPredicates();
        try {
            int result = databaseHelper.insert(uri,values);
            HiLog.warn(PersonDataAbility.LABEL_LOG, "persist %{public}d ",result);
            return  result > 0;
//            return mContext.getContentResolver().insert(uri, values) != null;
        } catch (Throwable e) {
            e.printStackTrace();
            HiLog.warn(PersonDataAbility.LABEL_LOG, "persist %{public}s ",e.toString());
            return false;
        }
    }

    /**
     * sends a query for TrayItems to the provider
     *
     * @param uri path to data
     * @return list of items
     * @throws TrayException when something is wrong with the provider/database
     */
    @NonNull
    public List<TrayItem> queryProvider(@NonNull final Uri uri) throws TrayException {
        DataAbilityHelper databaseHelper = DataAbilityHelper.creator(mContext);
        final ResultSet cursor;
        try {
            DataAbilityPredicates predicates = new DataAbilityPredicates();
            cursor = databaseHelper.query(uri, columns, predicates);
        } catch (Throwable e) {
            throw new TrayException("Hard error accessing the ContentProvider", e);
        }

        // Return Preference if found
        if (cursor == null) {
            // When running in here, please check if your ContentProvider has the correct authority
            throw new TrayException("could not access stored data with uri " + uri);
        }

        final ArrayList<TrayItem> list = new ArrayList<>();
        for (boolean hasItem = cursor.goToFirstRow(); hasItem; hasItem = cursor.goToNextRow()) {
            final TrayItem trayItem = cursorToTrayItem(cursor);
            list.add(trayItem);
        }
        cursor.close();
        return list;
    }

    /**
     * sends a query for TrayItems to the provider, doesn't throw when the database access couldn't
     * be established
     *
     * @param uri path to data
     * @return list of items, empty when an error occured
     */
    @NonNull
    public List<TrayItem> queryProviderSafe(@NonNull final Uri uri) {
        try {
            HiLog.warn(LABEL, "importedProviderSafe %{public}d , items: %{private}s ",1,"ss");
            return queryProvider(uri);
        } catch (TrayException e) {
            return new ArrayList<>();
        }
    }

    /**
     * removes items for the given Uri
     *
     * @param uri what to remove, use {@link TrayUri#builder()} to build a valid uri
     * @return true when delete runs without error. doesn't care about the delete result int
     */
    public boolean remove(final Uri uri) {
        try {
            DataAbilityHelper databaseHelper = DataAbilityHelper.creator(mContext);
//            mContext.getContentResolver().delete(uri, null, null);
            databaseHelper.delete(uri, new DataAbilityPredicates());
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * removes items for the given Uri and returns the count of the deleted items
     *
     * @param uri what to remove, use {@link TrayUri#builder()} to build a valid uri
     * @return number of deleted rows
     */
    public int removeAndCount(final Uri uri) {
        try {
            DataAbilityHelper databaseHelper = DataAbilityHelper.creator(mContext);
            return databaseHelper.delete(uri, new DataAbilityPredicates());
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * wipes all data, including meta data for the preferences like the current version number.
     *
     * @return true for success
     */
    public boolean wipe() {
        if (!clear()) {
            return false;
        }
        DataAbilityHelper databaseHelper = DataAbilityHelper.creator(mContext);
        try {
//          return mContext.getContentResolver().delete(mTrayUri.getInternal(), null, null) > 0;
            return databaseHelper.delete(mTrayUri.getInternal(), null) > 0;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * converts a {@link ResultSet} to a {@link TrayItem}
     * <p>
     * This is not a secondary constructor in {@link TrayItem} because the columns are a
     * implementation detail of the provider package
     *
     * @param cursor (size > 1)
     * @return a {@link TrayItem} filled with data
     */
    @NonNull
    static TrayItem cursorToTrayItem(final ResultSet  cursor) {
        final String module = cursor.getString(cursor
                .getColumnIndexForName(TrayContract.Preferences.Columns.MODULE));
        final String key = cursor.getString(cursor
                .getColumnIndexForName(TrayContract.Preferences.Columns.KEY));
        final String migratedKey = cursor.getString(cursor
                .getColumnIndexForName(TrayContract.Preferences.Columns.MIGRATED_KEY));
        final String value = cursor.getString(cursor
                .getColumnIndexForName(TrayContract.Preferences.Columns.VALUE));
        final Date created = new Date(cursor.getLong(cursor
                .getColumnIndexForName(TrayContract.Preferences.Columns.CREATED)));
        final Date updated = new Date(cursor.getLong(cursor
                .getColumnIndexForName(TrayContract.Preferences.Columns.UPDATED)));
        return new TrayItem(module, key, migratedKey, value, created, updated);
    }
}
