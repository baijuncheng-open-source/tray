package net.grandcentrix.tray.sample;

import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.core.*;
import net.grandcentrix.tray.provider.ContentProviderStorage;

import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.utils.net.Uri;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, Preferences.class, TrayLog.class, HiLog.class, TextTool.class})
public class TrayPreferencesTest {
    TrayPreferences trayPreferences;
    ContentProviderStorage ContentProviderStorage;

    @Before
    public void setUp() throws Exception {
        Context context = mock(Context.class);
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        ContentProviderStorage = mock(ContentProviderStorage.class);
        Preferences Preferences = mock(Preferences.class);
        PowerMockito.whenNew(ContentProviderStorage.class).withAnyArguments().thenReturn(ContentProviderStorage);
        PowerMockito.whenNew(Preferences.class).withAnyArguments().thenReturn(Preferences);

        trayPreferences =
                new TrayPreferences(context, "nobackup", 1, TrayStorage.Type.DEVICE);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test_getBoolean() {
        Assert.assertFalse(trayPreferences.getBoolean("jh", false));
    }

    @Test
    public void test_getBoolean_caseNull() {
        Assert.assertFalse(trayPreferences.getBoolean(null, false));
    }

    @Test
    public void test_getFloat() {
        Assert.assertTrue(-1f == trayPreferences.getFloat("jh", -1f));
    }

    @Test
    public void test_getFloat_caseNull() {
        Assert.assertTrue(-1f == trayPreferences.getFloat(null, -1f));
    }

    @Test
    public void test_getInt() {
        Assert.assertTrue(-1 == trayPreferences.getInt("jh", -1));
    }

    @Test
    public void test_getInt_caseNull() {
        Assert.assertTrue(-1 == trayPreferences.getInt(null, -1));
    }

    @Test
    public void test_getLong() {
        Assert.assertTrue(-1 == trayPreferences.getLong("jh", -1L));
    }

    @Test
    public void test_getLong_caseNull() {
        Assert.assertTrue(-1 == trayPreferences.getLong(null, -1L));
    }

    @Test
    public void test_getName() {
        Assert.assertNull(trayPreferences.getName());
    }

    @Test
    public void test_getString() {
        Assert.assertEquals(trayPreferences.getString("jh", "default"), "default");
    }

    @Test
    public void test_registerOnTrayPreferenceChangeListener() {
        OnTrayPreferenceChangeListener onTrayPreferenceChangeListener = new OnTrayPreferenceChangeListener() {
            @Override
            public void onTrayPreferenceChanged(Collection<TrayItem> items) {

            }
        };
        trayPreferences.registerOnTrayPreferenceChangeListener(onTrayPreferenceChangeListener);
        Mockito.verify(ContentProviderStorage).registerOnTrayPreferenceChangeListener(onTrayPreferenceChangeListener);


    }

    @Test
    public void test_unregisterOnTrayPreferenceChangeListener() {
        OnTrayPreferenceChangeListener onTrayPreferenceChangeListener = new OnTrayPreferenceChangeListener() {
            @Override
            public void onTrayPreferenceChanged(Collection<TrayItem> items) {

            }
        };
        trayPreferences.unregisterOnTrayPreferenceChangeListener(onTrayPreferenceChangeListener);
        Mockito.verify(ContentProviderStorage).unregisterOnTrayPreferenceChangeListener(onTrayPreferenceChangeListener);
    }

    @Test
    public void test_clear() {
        Assert.assertFalse(trayPreferences.clear());
    }

    @Test
    public void test_contains() {
        Assert.assertFalse(trayPreferences.contains("jh"));
    }

    @Test
    public void test_getAll() {
        trayPreferences.getAll();
        Mockito.verify(ContentProviderStorage).getAll();
    }

    @Test
    public void test_getPref() {
        trayPreferences.getPref("key");
        Mockito.verify(ContentProviderStorage).get("key");
    }

    @Test
    public void test_getVersion() {
        try {
            trayPreferences.getVersion();
            Mockito.verify(ContentProviderStorage, Mockito.atLeastOnce()).getVersion();
        } catch (TrayException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_migrate() {
        SharedPreferencesImport sharedPreferencesImport = mock(SharedPreferencesImport.class);
        when(sharedPreferencesImport.shouldMigrate()).thenReturn(true);
        when(sharedPreferencesImport.getData()).thenReturn(true);

        trayPreferences.migrate(new Migration[]{sharedPreferencesImport});
        Mockito.verify(sharedPreferencesImport, Mockito.atLeastOnce()).onPostMigrate(null);
    }

    @Test
    public void test_migrateMigrateNone() {
        SharedPreferencesImport sharedPreferencesImport = mock(SharedPreferencesImport.class);
        when(sharedPreferencesImport.shouldMigrate()).thenReturn(false);
        when(sharedPreferencesImport.getData()).thenReturn(true);
        PowerMockito.mockStatic(TrayLog.class);
        trayPreferences.migrate(new Migration[]{sharedPreferencesImport});
        PowerMockito.verifyStatic(TrayLog.class, Mockito.atLeastOnce());
        TrayLog.v(any());
    }

    @Test
    public void test_migrateMigrateSupportedDataType() {
        SharedPreferencesImport sharedPreferencesImport = mock(SharedPreferencesImport.class);
        when(sharedPreferencesImport.shouldMigrate()).thenReturn(true);
        when(sharedPreferencesImport.getData()).thenReturn('C');
        PowerMockito.mockStatic(TrayLog.class);
        trayPreferences.migrate(new Migration[]{sharedPreferencesImport});
        PowerMockito.verifyStatic(TrayLog.class, Mockito.atLeastOnce());
        TrayLog.w(any());
    }

    @Test
    public void test_putString() {
        PowerMockito.mockStatic(TextTool.class);
        PowerMockito.when(TextTool.isNullOrEmpty("key1")).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                String params = invocationOnMock.getArgument(0);
                if (params.equals("key1")) {
                    return false;
                }
                return true;
            }
        });
        boolean result = trayPreferences.put("key1", "value1");
        Assert.assertFalse(result);
    }

    @Test
    public void test_PutBoolean() {
        PowerMockito.mockStatic(TextTool.class);
        PowerMockito.when(TextTool.isNullOrEmpty("key1")).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                String params = invocationOnMock.getArgument(0);
                if (params.equals("key1")) {
                    return false;
                }
                return true;
            }
        });
        boolean result = trayPreferences.put("key1", true);
        Assert.assertFalse(result);
    }

    @Test
    public void test_testPutFloat() {
        PowerMockito.mockStatic(TextTool.class);
        PowerMockito.when(TextTool.isNullOrEmpty("key1")).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                String params = invocationOnMock.getArgument(0);
                if (params.equals("key1")) {
                    return false;
                }
                return true;
            }
        });
        boolean result = trayPreferences.put("key1", 1f);
        Assert.assertFalse(result);
    }

    @Test
    public void test_PutLong() {
        PowerMockito.mockStatic(TextTool.class);
        PowerMockito.when(TextTool.isNullOrEmpty("key1")).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                String params = invocationOnMock.getArgument(0);
                if (params.equals("key1")) {
                    return false;
                }
                return true;
            }
        });
        boolean result = trayPreferences.put("key1", 1l);
        Assert.assertFalse(result);
    }

    @Test
    public void test_PutInt() {
        PowerMockito.mockStatic(TextTool.class);
        PowerMockito.when(TextTool.isNullOrEmpty("key1")).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                String params = invocationOnMock.getArgument(0);
                if (params.equals("key1")) {
                    return false;
                }
                return true;
            }
        });
        boolean result = trayPreferences.put("key1", 5);
        Assert.assertFalse(result);
    }

    @Test
    public void test_remove() {
        Assert.assertFalse(trayPreferences.remove("key1"));
    }

    @Test
    public void test_wipe() {
        Assert.assertFalse(trayPreferences.wipe());
    }


    @Test
    public void test_annexModule() {
        trayPreferences.annexModule("name");
        PowerMockito.verifyStatic(TrayLog.class, Mockito.atLeastOnce());
        TrayLog.v(any());
    }

    @Test
    public void test_testAnnexModule() {
        PowerMockito.mockStatic(TrayLog.class);
        trayPreferences.annexModule("storageNameA");

        PowerMockito.verifyStatic(TrayLog.class);
        TrayLog.v(any());
    }

    @Test
    public void test_getContext() {
        Class c = trayPreferences.getClass();
        try {
            Method method = c.getDeclaredMethod("getContext");
            method.setAccessible(true);
            Object result = method.invoke(trayPreferences);
            Assert.assertNull(result);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    @Test(expected = net.grandcentrix.tray.core.WrongTypeException.class)
    public void throwForNullValue() throws Exception {
        AbstractTrayPreference abstractTrayPreference = mock(AbstractTrayPreference.class);

        try {
            doCallRealMethod().when(abstractTrayPreference, "throwForNullValue", any(), any(), any());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Whitebox.invokeMethod(abstractTrayPreference, "throwForNullValue", new Class[]{String.class, Class.class, String.class}, null, AbstractTrayPreference.class, "key");


    }


}