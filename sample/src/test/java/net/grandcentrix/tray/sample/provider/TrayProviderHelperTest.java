package net.grandcentrix.tray.sample.provider;

import net.grandcentrix.tray.PersonDataAbility;
import net.grandcentrix.tray.sample.og.ImportTrayPreferences;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.core.TrayException;
import net.grandcentrix.tray.core.TrayItem;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.ContentProviderStorage;
import net.grandcentrix.tray.provider.TrayContract;
import net.grandcentrix.tray.provider.TrayProviderHelper;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, TrayProviderHelper.class, TrayLog.class, HiLog.class, TextTool.class, TrayContract.class, ContentProviderStorage.class, HiLogLabel.class, Log.class, EventRunner.class, DataAbilityHelper.class, ImportTrayPreferences.class, PersonDataAbility.class})
public class TrayProviderHelperTest {
    TrayProviderHelper trayProviderHelper;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        PowerMockito.mockStatic(HiLogLabel.class);
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(TrayLog.class);
        Context context = mock(Context.class);
        when(context.getApplicationContext()).thenReturn(context);
        PowerMockito.mockStatic(Uri.class);
        Uri uri = mock(Uri.class);
        Uri.Builder builder = mock(Uri.Builder.class);
        when(uri.makeBuilder()).thenReturn(builder);
        when(builder.build()).thenReturn(uri);


        when(Uri.parse(any())).thenReturn(uri);

        PowerMockito.mockStatic(TrayContract.class);
        when(TrayContract.generateContentUri(any())).thenReturn(uri);
        when(TrayContract.class, "generateInternalContentUri", any()).thenReturn(uri);
        when(uri.makeBuilder()).thenReturn(builder);


        trayProviderHelper = new TrayProviderHelper(context);
    }

    @Test
    public void test_clear() {
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        doNothing().when(dataAbilityHelper).registerObserver(any(), any());

        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }

        trayProviderHelper.clear();
        try {
            Mockito.verify(dataAbilityHelper, Mockito.atLeastOnce()).delete(any(), any());
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_clearBut() {
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        doNothing().when(dataAbilityHelper).registerObserver(any(), any());
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }

        trayProviderHelper.clearBut(mock(ImportTrayPreferences.class), mock(ImportTrayPreferences.class));
        try {
            Mockito.verify(dataAbilityHelper, Mockito.atLeastOnce()).delete(any(), any());
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_clearBut_withNull() {
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        doNothing().when(dataAbilityHelper).registerObserver(any(), any());
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }

        trayProviderHelper.clearBut(mock(ImportTrayPreferences.class), null);
        try {
            Mockito.verify(dataAbilityHelper, Mockito.atLeastOnce()).delete(any(), any());
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_getAll() {
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        Assert.assertNotNull(trayProviderHelper.getAll());
    }

    @Test
    public void test_persist() {
        ValuesBucket valuesBucket = mock(ValuesBucket.class);
        try {
            whenNew(ValuesBucket.class).withAnyArguments().thenReturn(valuesBucket);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertFalse(trayProviderHelper.persist("module1", "key1", "time"));
    }

    @Test
    public void test_Persist() {
        ValuesBucket valuesBucket = mock(ValuesBucket.class);
        try {
            whenNew(ValuesBucket.class).withAnyArguments().thenReturn(valuesBucket);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = mock(Uri.class);
        Assert.assertFalse(trayProviderHelper.persist(uri, "module1"));
    }

    @Test
    public void test_queryProvider() {
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = mock(Uri.class);
        ResultSet resultSet = mock(ResultSet.class);
        try {
            when(dataAbilityHelper.query(any(), any(), any())).thenReturn(resultSet);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        try {
            Assert.assertTrue(trayProviderHelper.queryProvider(uri).size() >= 0);
        } catch (TrayException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test_queryProviderSafe() {
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = mock(Uri.class);
        ResultSet resultSet = mock(ResultSet.class);
        try {
            when(dataAbilityHelper.query(any(), any(), any())).thenReturn(resultSet);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(trayProviderHelper.queryProviderSafe(uri).size() >= 0);
    }

    @Test
    public void test_remove() {
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = mock(Uri.class);
        ResultSet resultSet = mock(ResultSet.class);
        try {
            when(dataAbilityHelper.query(any(), any(), any())).thenReturn(resultSet);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(trayProviderHelper.remove(uri));
    }

    @Test
    public void test_remove_caseNull() {
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = mock(Uri.class);
        ResultSet resultSet = mock(ResultSet.class);
        try {
            when(dataAbilityHelper.query(any(), any(), any())).thenReturn(resultSet);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(trayProviderHelper.remove(null));
    }

    @Test
    public void test_removeAndCount() {
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = mock(Uri.class);
        ResultSet resultSet = mock(ResultSet.class);
        try {
            when(dataAbilityHelper.query(any(), any(), any())).thenReturn(resultSet);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(trayProviderHelper.removeAndCount(uri) == 0);
    }

    @Test
    public void test_wipe() {
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        try {
            whenNew(DataAbilityPredicates.class).withAnyArguments().thenReturn(dataAbilityPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = mock(Uri.class);
        ResultSet resultSet = mock(ResultSet.class);
        try {
            when(dataAbilityHelper.query(any(), any(), any())).thenReturn(resultSet);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        Assert.assertFalse(trayProviderHelper.wipe());
    }

    @Test
    public void test_cursorToTrayItem() {
        Class<?> threadClazz = null;
        threadClazz = trayProviderHelper.getClass();
        Method method = null;
        try {
            method = threadClazz.getDeclaredMethod("cursorToTrayItem", ResultSet.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        method.setAccessible(true);

        ResultSet cursor = mock(ResultSet.class);

        try {
            TrayItem trayItem = (TrayItem) method.invoke(null, new Object[]{cursor});
            Assert.assertNotNull(trayItem);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}