package net.grandcentrix.tray.sample.core;

import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.core.SharedPreferencesImport;
import net.grandcentrix.tray.core.TrayItem;
import net.grandcentrix.tray.core.TrayLog;
import ohos.agp.utils.TextTool;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Date;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, Preferences.class, TrayLog.class, HiLog.class, TextTool.class, DatabaseHelper.class, SharedPreferencesImport.class})
public class TrayItemTest {
    TrayItem trayItem;

    @Before
    public void setUp() throws Exception {
        trayItem = new TrayItem("module", "key", "migratedKey", "value", new Date(), new Date());

    }

    @Test
    public void test_created() {
        Date date = trayItem.created();
        Assert.assertTrue(date != null);

    }

    @Test
    public void test_key() {
        Assert.assertEquals("key", trayItem.key());
    }

    @Test
    public void test_migratedKey() {
        Assert.assertEquals("migratedKey", trayItem.migratedKey());
    }

    @Test
    public void test_module() {
        Assert.assertEquals("module", trayItem.module());
    }

    @Test
    public void test_toString() {
        Assert.assertTrue(trayItem.toString().contains("update"));
    }

    @Test
    public void test_updateTime() {
        Assert.assertTrue(trayItem.updateTime() != null);
        ;
    }

}