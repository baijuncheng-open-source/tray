package net.grandcentrix.tray.sample.core;

import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.core.SharedPreferencesImport;
import net.grandcentrix.tray.core.TrayLog;
import ohos.agp.utils.TextTool;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.ArgumentMatchers.*;

@FixMethodOrder(MethodSorters.JVM)
@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class, HiLogLabel.class, Preferences.class, TrayLog.class, HiLog.class, TextTool.class, DatabaseHelper.class, SharedPreferencesImport.class})
public class TrayLogTest {

    @Test
    public void test_d() {
        HiLogLabel hiLogLabel = PowerMockito.mock(HiLogLabel.class);
        try {
            PowerMockito.whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(HiLog.class);
        TrayLog.d(null);
        PowerMockito.verifyStatic(Log.class);
        Log.d(any(), any());
    }

    @Test
    public void test_e() {
        HiLogLabel hiLogLabel = PowerMockito.mock(HiLogLabel.class);
        try {
            PowerMockito.whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(HiLog.class);
        TrayLog.e(null);
        PowerMockito.verifyStatic(Log.class);
        Log.e(any(), any());
    }

    @Test
    public void test_E() {
        HiLogLabel hiLogLabel = PowerMockito.mock(HiLogLabel.class);
        try {
            PowerMockito.whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(HiLog.class);
        TrayLog.e(new Throwable("th"), "throw");
        PowerMockito.verifyStatic(Log.class);
        Log.e(any(), any(), any());
    }


    @Test
    public void test_setTag() {
        HiLogLabel hiLogLabel = PowerMockito.mock(HiLogLabel.class);
        try {
            PowerMockito.whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(HiLog.class);
        TrayLog.setTag("throw");
        PowerMockito.verifyStatic(Log.class);
        Log.d(any(), any());
    }

    @Test
    public void test_w() {
        HiLogLabel hiLogLabel = PowerMockito.mock(HiLogLabel.class);
        try {
            PowerMockito.whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.when(HiLog.isLoggable(anyInt(), anyString(), anyInt())).thenReturn(true);
        TrayLog.w(null);
        PowerMockito.verifyStatic(Log.class);
        Log.w(anyString(), anyString());
    }

    @Test
    public void test_wtf() {
        HiLogLabel hiLogLabel = PowerMockito.mock(HiLogLabel.class);
        try {
            PowerMockito.whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.when(HiLog.isLoggable(anyInt(), anyString(), anyInt())).thenReturn(true);
        TrayLog.wtf(null);
        PowerMockito.verifyStatic(Log.class);
        Log.d(anyString(), anyString());
    }

    @Test
    public void test_v() {
        HiLogLabel hiLogLabel = PowerMockito.mock(HiLogLabel.class);
        try {
            PowerMockito.whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(HiLog.class);
        TrayLog.DEBUG = true;
        TrayLog.v(null);
        PowerMockito.verifyStatic(Log.class);
        Log.v(any(), any());
        TrayLog.DEBUG = false;
    }
}