package net.grandcentrix.tray.sample.core;

import net.grandcentrix.tray.core.WrongTypeException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WrongTypeExceptionTest {

    @Before
    public void setUp() throws Exception {
    }


    @Test
    public void test_WrongTypeException() {
        Assert.assertNotNull(new WrongTypeException());
    }

    @Test
    public void test_WrongTypeExceptionParams() {
        Assert.assertNotNull(new WrongTypeException("error", this));
    }

    @Test
    public void test_WrongTypeExceptionParams2() {
        Assert.assertNotNull(new WrongTypeException("error"));
    }

    @Test
    public void test_WrongTypeExceptionParamsThrows() {
        Assert.assertNotNull(new WrongTypeException("error", new Throwable()));
    }


}