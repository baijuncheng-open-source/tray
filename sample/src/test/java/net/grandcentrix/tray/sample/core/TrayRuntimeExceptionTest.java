package net.grandcentrix.tray.sample.core;

import net.grandcentrix.tray.core.TrayRuntimeException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TrayRuntimeExceptionTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void test_TrayRuntimeException() {
        Assert.assertNotNull(new TrayRuntimeException());

    }

    @Test
    public void test_TrayRuntimeExceptionParams() throws Exception {
        Assert.assertNotNull(new TrayRuntimeException("Params"));
    }

    @Test
    public void test_TrayRuntimeExceptionParams2() {
        Assert.assertNotNull(new TrayRuntimeException("Params", this));
    }

    @Test
    public void test_TrayRuntimeExceptionThrowable() throws Exception {
        Assert.assertNotNull(new TrayRuntimeException("Params", new Throwable()));
    }


}