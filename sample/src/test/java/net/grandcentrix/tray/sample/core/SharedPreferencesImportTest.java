package net.grandcentrix.tray.sample.core;

import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.core.SharedPreferencesImport;
import net.grandcentrix.tray.core.TrayItem;
import net.grandcentrix.tray.core.TrayLog;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Date;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, Preferences.class, TrayLog.class, HiLog.class, TextTool.class, DatabaseHelper.class, SharedPreferencesImport.class})
public class SharedPreferencesImportTest {
    SharedPreferencesImport userTokenMigration;
    Preferences preferences;

    @Before
    public void setUp() throws Exception {
        Context context = mock(Context.class);
        preferences = mock(Preferences.class);
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);
        when(databaseHelper.getPreferences("SHARED_PREF_NAME")).thenReturn(preferences);
        when(preferences.hasKey("str")).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                String params = invocationOnMock.getArgument(0);
                return params.equals("userToken");
            }
        });
        userTokenMigration = new SharedPreferencesImport(context,
                "SHARED_PREF_NAME", "userToken", "KEY_USER_TOKEN");
    }

    @Test
    public void test_getData() {
        userTokenMigration.getData();
        Mockito.verify(preferences).getAll();

    }

    @Test
    public void test_getPreviousKey() {
        Assert.assertEquals("userToken", userTokenMigration.getPreviousKey());

    }

    @Test
    public void test_getTrayKey() {
        Assert.assertEquals("KEY_USER_TOKEN", userTokenMigration.getTrayKey());
    }

    @Test
    public void test_onPostMigrate() {
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        userTokenMigration.onPostMigrate(null);
        verifyStatic(TrayLog.class);
        TrayLog.wtf(any());
    }

    @Test
    public void test_onPostMigrate_with_Normal() {
        TrayItem trayItem = new TrayItem("module", "key", "migratedKey", "value", new Date(), new Date());
        HashMap map = new HashMap<>();
        map.put("userToken", "value");
        when(preferences.getAll()).thenReturn(map);
        when(preferences.delete(any())).thenReturn(preferences);
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        userTokenMigration.onPostMigrate(trayItem);
        Mockito.verify(preferences).flush();
    }

    @Test
    public void test_shouldMigrate() {
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        when(preferences.hasKey(any())).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                String params = invocationOnMock.getArgument(0);
                return params.equals("userToken");
            }
        });
        Assert.assertEquals(true, userTokenMigration.shouldMigrate());
    }

    @Test
    public void test_shouldNotMigrate() {
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        when(preferences.hasKey(any())).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                String params = invocationOnMock.getArgument(0);
                return !params.equals("userToken");
            }
        });
        Assert.assertEquals(false, userTokenMigration.shouldMigrate());
    }
}