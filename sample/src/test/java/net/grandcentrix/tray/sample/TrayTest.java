package net.grandcentrix.tray.sample;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.Tray;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.core.Preferences;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.ContentProviderStorage;
import net.grandcentrix.tray.provider.TrayProviderHelper;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.utils.net.Uri;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, Uri.class, TrayPreferences.class, Preferences.class, TrayLog.class, HiLog.class, Tray.class})
public class TrayTest {
    Tray tray;
    TrayProviderHelper trayProviderHelper;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        PowerMockito.mockStatic(Uri.class);
        ContentProviderStorage ContentProviderStorage = mock(ContentProviderStorage.class);
        Preferences Preferences = mock(Preferences.class);
        PowerMockito.whenNew(ContentProviderStorage.class).withAnyArguments().thenReturn(ContentProviderStorage);
        PowerMockito.whenNew(Preferences.class).withAnyArguments().thenReturn(Preferences);

        trayProviderHelper = mock(TrayProviderHelper.class);
        whenNew(TrayProviderHelper.class).withAnyArguments().thenReturn(trayProviderHelper);
        tray = new Tray(mock(Context.class));
    }

    @Test
    public void test_clear() {
        AppPreferences preferences = new AppPreferences(mock(Context.class));
        Tray.clear(null, preferences);
        PowerMockito.verifyStatic(TrayLog.class, Mockito.atLeast(1));
        TrayLog.v(any());
    }

    @Test
    public void test_testClear() {
        tray.clear();
        Mockito.verify(trayProviderHelper).clear();
    }

    @Test
    public void test_clearBut() {
        tray.clearBut(null);
        Mockito.verify(trayProviderHelper).clearBut(null);

    }

    @Test
    public void test_getAll() {
        tray.getAll();
        Mockito.verify(trayProviderHelper).getAll();
    }

    @Test
    public void test_wipe() {
        tray.wipe();
        Mockito.verify(trayProviderHelper).wipe();
    }
}