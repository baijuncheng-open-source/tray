package net.grandcentrix.tray.sample;


import net.grandcentrix.tray.PersonDataAbility;
import net.grandcentrix.tray.Tray;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.UriMatcher;
import net.grandcentrix.tray.core.Preferences;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.SqliteHelper;
import net.grandcentrix.tray.provider.TrayDBHelper;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.app.AbilityContext;
import ohos.app.Context;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, Uri.class, TrayPreferences.class, Preferences.class, TrayLog.class, HiLog.class, Tray.class, HiLogLabel.class, PersonDataAbility.class, SqliteHelper.class, DataAbilityHelper.class, AbilityContext.class, SqliteHelper.class})
public class PersonDataAbilityTest {
    UriMatcher uriMatcher;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        PowerMockito.mockStatic(Uri.class);

        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }


        uriMatcher = mock(UriMatcher.class);
        whenNew(UriMatcher.class).withAnyArguments().thenReturn(uriMatcher);

        Class<?> threadClazz = null;
        try {
            threadClazz = Class.forName("net.grandcentrix.tray.PersonDataAbility");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Method[] methods = threadClazz.getMethods();
        Method method = null;
        try {
            method = threadClazz.getDeclaredMethod("setAuthority", String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        method.setAccessible(true);
        try {
            method.invoke(null, new Object[]{"net.grandcentrix.tray.PersonDataAbility"});
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }



    @Test
    public void test_query() {
        PowerMockito.mockStatic(SqliteHelper.class);

        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        try {
            whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }

        when(uriMatcher.match(any())).thenReturn(10);
        PersonDataAbility personDataAbility = Mockito.mock(PersonDataAbility.class);
        Mockito.doCallRealMethod().when(personDataAbility).query(any(), any(), any());
        Uri uri = mock(Uri.class);
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        when(dataAbilityPredicates.getWhereClause()).thenReturn("");
        when(uri.getDecodedPathList()).thenReturn(Arrays.asList(new String[]{"1", "2", "3"}.clone()));
        when(uri.getFirstQueryParamByKey(any())).thenReturn("backup");

        RdbStore rdbStore = mock(RdbStore.class);
        when(personDataAbility.getReadableDatabase(any())).thenReturn(rdbStore);


        Assert.assertNull(personDataAbility.query(uri, new String[]{"time", "one"}, dataAbilityPredicates));

    }

    @Test(expected = NullPointerException.class)
    public void test_query_null() {
        PowerMockito.mockStatic(SqliteHelper.class);

        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        try {
            whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }

        when(uriMatcher.match(any())).thenReturn(10);
        PersonDataAbility personDataAbility = Mockito.mock(PersonDataAbility.class);
        Mockito.doCallRealMethod().when(personDataAbility).query(any(), any(), any());
        Uri uri = mock(Uri.class);
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        when(dataAbilityPredicates.getWhereClause()).thenReturn("");
        when(uri.getDecodedPathList()).thenReturn(Arrays.asList(new String[]{"1", "2", "3"}.clone()));
        when(uri.getFirstQueryParamByKey(any())).thenReturn("backup");

        RdbStore rdbStore = mock(RdbStore.class);
        when(personDataAbility.getReadableDatabase(any())).thenReturn(rdbStore);


        Assert.assertNull(personDataAbility.query(null, null, null));

    }


    @Test
    public void test_insert() {
        Uri uri = mock(Uri.class);
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        when(dataAbilityPredicates.getWhereClause()).thenReturn("");
        when(uri.getDecodedPathList()).thenReturn(Arrays.asList(new String[]{"1", "2", "3"}.clone()));
        when(uri.getFirstQueryParamByKey(any())).thenReturn("backup");
        when(uriMatcher.match(any())).thenReturn(10);
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).insert(any(), any());

        ValuesBucket bucket = mock(ValuesBucket.class);

        PowerMockito.mockStatic(DataAbilityHelper.class);

        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        Assert.assertTrue(personDataAbility.insert(uri, bucket) >= 0);

    }


    @Test(expected = NullPointerException.class)
    public void test_insert_null() {
        Uri uri = mock(Uri.class);
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        when(dataAbilityPredicates.getWhereClause()).thenReturn("");
        when(uri.getDecodedPathList()).thenReturn(Arrays.asList(new String[]{"1", "2", "3"}.clone()));
        when(uri.getFirstQueryParamByKey(any())).thenReturn("backup");
        when(uriMatcher.match(any())).thenReturn(10);
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).insert(any(), any());

        ValuesBucket bucket = mock(ValuesBucket.class);

        PowerMockito.mockStatic(DataAbilityHelper.class);

        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        Assert.assertTrue(personDataAbility.insert(null,null) >= 0);

    }

    @Test
    public void test_insert_WithCase() {
        Uri uri = mock(Uri.class);
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        when(dataAbilityPredicates.getWhereClause()).thenReturn("");
        when(uri.getDecodedPathList()).thenReturn(Arrays.asList(new String[]{"1", "2", "3"}.clone()));
        when(uri.getFirstQueryParamByKey(any())).thenReturn("backup");
        when(uriMatcher.match(any())).thenReturn(10);
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).insert(any(), any());

        ValuesBucket bucket = mock(ValuesBucket.class);

        PowerMockito.mockStatic(DataAbilityHelper.class);

        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        when(personDataAbility.insertOrUpdate(any(), any(), any(), any(), any(), any())).thenReturn(-1);


        Assert.assertTrue(personDataAbility.insert(uri, bucket) < 0);

    }

    @Test
    public void test_delete() {
        PowerMockito.mockStatic(SqliteHelper.class);
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).delete(any(), any());
        Uri uri = mock(Uri.class);
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        when(uriMatcher.match(any())).thenReturn(10);
        when(dataAbilityPredicates.getWhereClause()).thenReturn("");
        when(uri.getDecodedPathList()).thenReturn(Arrays.asList(new String[]{"1", "2", "3"}.clone()));
        when(uri.getFirstQueryParamByKey(any())).thenReturn("backup");

        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        try {
            whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RdbStore rdbStore = mock(RdbStore.class);
        when(personDataAbility.getReadableDatabase(any())).thenReturn(rdbStore);

        Assert.assertTrue(personDataAbility.delete(uri, dataAbilityPredicates) <= 0);
    }


    @Test(expected = NullPointerException.class)
    public void test_delete_case_null() {
        PowerMockito.mockStatic(SqliteHelper.class);
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).delete(any(), any());
        Uri uri = mock(Uri.class);
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        when(uriMatcher.match(any())).thenReturn(10);
        when(dataAbilityPredicates.getWhereClause()).thenReturn("");
        when(uri.getDecodedPathList()).thenReturn(Arrays.asList(new String[]{"1", "2", "3"}.clone()));
        when(uri.getFirstQueryParamByKey(any())).thenReturn("backup");

        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        try {
            whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RdbStore rdbStore = mock(RdbStore.class);
        when(personDataAbility.getReadableDatabase(any())).thenReturn(rdbStore);

        Assert.assertTrue(personDataAbility.delete(null, null) <= 0);
    }

    @Test
    public void test_deleteWithCase_delete_return_one() {
        PowerMockito.mockStatic(SqliteHelper.class);
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).delete(any(), any());
        Uri uri = mock(Uri.class);
        DataAbilityPredicates dataAbilityPredicates = mock(DataAbilityPredicates.class);
        when(uriMatcher.match(any())).thenReturn(10);
        when(dataAbilityPredicates.getWhereClause()).thenReturn("");
        when(uri.getDecodedPathList()).thenReturn(Arrays.asList(new String[]{"1", "2", "3"}.clone()));
        when(uri.getFirstQueryParamByKey(any())).thenReturn("backUP");

        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        try {
            whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RdbStore rdbStore = mock(RdbStore.class);

        when(personDataAbility.getReadableDatabase(any())).thenReturn(rdbStore);
        when(rdbStore.delete(any())).thenReturn(1);
        PowerMockito.mockStatic(DataAbilityHelper.class);

        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);

        Assert.assertTrue(personDataAbility.delete(uri, dataAbilityPredicates) > 0);
    }

    @Test
    public void test_update() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).update(any(), any(), any());
        Uri uri = mock(Uri.class);
        ValuesBucket bucket = mock(ValuesBucket.class);
        DataAbilityPredicates dataAbilityHelper = mock(DataAbilityPredicates.class);
        Assert.assertTrue(personDataAbility.update(uri, bucket, dataAbilityHelper) == 0);
    }

    @Test
    public void test_update_case_null() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).update(any(), any(), any());
        Uri uri = mock(Uri.class);
        ValuesBucket bucket = mock(ValuesBucket.class);
        DataAbilityPredicates dataAbilityHelper = mock(DataAbilityPredicates.class);
        Assert.assertTrue(personDataAbility.update(null, null, null) == 0);
    }

    @Test
    public void test_openFile() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).openFile(any(), any());
        Assert.assertNull(personDataAbility.openFile(null, null));

    }

    @Test
    public void test_getFileTypes() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).getFileTypes(any(), any());
        Assert.assertTrue(personDataAbility.getFileTypes(mock(Uri.class), "mimetype") instanceof String[]);
    }

    @Test
    public void test_getFileTypes_case_null() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).getFileTypes(any(), any());
        Assert.assertTrue(personDataAbility.getFileTypes(null, null) instanceof String[]);
    }


    @Test
    public void test_getType() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).getType(any());
        Assert.assertNull(personDataAbility.getType(null));
    }


    @Test
    public void test_getTableWithNull() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).getTable(any());
        Uri uri = mock(Uri.class);
        ValuesBucket bucket = mock(ValuesBucket.class);
        DataAbilityPredicates dataAbilityHelper = mock(DataAbilityPredicates.class);
        Assert.assertTrue(personDataAbility.getTable(null) == null);
    }

    @Test
    public void test_getTableWithSinglePreference() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).getTable(any());
        Uri uri = mock(Uri.class);
        when(uriMatcher.match(any())).thenReturn(10);
        Assert.assertTrue(personDataAbility.getTable(uri).equals(TrayDBHelper.TABLE_NAME));
    }

    @Test
    public void test_getTableWithInternalPreference() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).getTable(any());
        Uri uri = mock(Uri.class);
        when(uriMatcher.match(any())).thenReturn(110);
        Assert.assertTrue(personDataAbility.getTable(uri).equals(TrayDBHelper.INTERNAL_TABLE_NAME));
    }


    @Test
    public void test_getTableWithInternalPreference_caseNull() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).getTable(any());
        Uri uri = mock(Uri.class);
        when(uriMatcher.match(any())).thenReturn(110);
        Assert.assertTrue(personDataAbility.getTable(null)==null);
    }

    @Test
    public void test_insertOrUpdate() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).insertOrUpdate(any(), any(), any(), any(), any(), any());
        RdbStore rdbStore = mock(RdbStore.class);
        ValuesBucket valuesBucket = mock(ValuesBucket.class);
        PowerMockito.mockStatic(SqliteHelper.class);
        personDataAbility.insertOrUpdate(rdbStore, "table", "prefSelection", new String[]{""}, valuesBucket, new String[]{"m"});
        PowerMockito.verifyStatic(SqliteHelper.class, Mockito.atLeast(1));
        SqliteHelper.insertOrUpdate(any(), any(), any(), any(), any(), any());


    }


    @Test
    public void test_insertOrUpdate_case_null() {
        PersonDataAbility personDataAbility = mock(PersonDataAbility.class);
        Context context = mock(Context.class);
        when(personDataAbility.getContext()).thenReturn(context);
        Mockito.doCallRealMethod().when(personDataAbility).insertOrUpdate(any(), any(), any(), any(), any(), any());
        RdbStore rdbStore = mock(RdbStore.class);
        ValuesBucket valuesBucket = mock(ValuesBucket.class);
        PowerMockito.mockStatic(SqliteHelper.class);
        personDataAbility.insertOrUpdate(null, null, null, null, null, null);
        PowerMockito.verifyStatic(SqliteHelper.class, Mockito.atLeast(1));
        SqliteHelper.insertOrUpdate(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void test_setAuthority() {
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Class<?> threadClazz = null;

        try {
            threadClazz = Class.forName("net.grandcentrix.tray.PersonDataAbility");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Method[] methods = threadClazz.getMethods();
        Method method = null;
        try {
            method = threadClazz.getDeclaredMethod("setAuthority", String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        method.setAccessible(true);
        try {
            method.invoke(null, new Object[]{"setAuthority"});
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Field field = null;
        try {
            field = threadClazz.getDeclaredField("sURIMatcher");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        field.setAccessible(true);
        Assert.assertNotNull(field);

    }


    @Test(expected = IllegalArgumentException.class)
    public void test_setAuthority_caseNull() {
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Class<?> threadClazz = null;

        try {
            threadClazz = Class.forName("net.grandcentrix.tray.PersonDataAbility");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Method[] methods = threadClazz.getMethods();
        Method method = null;
        try {
            method = threadClazz.getDeclaredMethod("setAuthority", String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        method.setAccessible(true);
        try {
            method.invoke(null, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Field field = null;
        try {
            field = threadClazz.getDeclaredField("sURIMatcher");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        field.setAccessible(true);
        Assert.assertNotNull(field);

    }
}