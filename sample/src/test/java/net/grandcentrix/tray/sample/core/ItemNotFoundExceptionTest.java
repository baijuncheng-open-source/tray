package net.grandcentrix.tray.sample.core;

import net.grandcentrix.tray.core.ItemNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ItemNotFoundExceptionTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void test_ItemNotFoundException() throws Exception {
        Assert.assertNotNull(new ItemNotFoundException());

    }

    @Test
    public void test_ItemNotFoundException_withParams() throws Exception {
        Assert.assertNotNull(new ItemNotFoundException("detailMessage", this));

    }

    @Test
    public void test_ItemNotFoundException_withParamSize2() throws Exception {
        Assert.assertNotNull(new ItemNotFoundException("detailMessage", new Throwable("error")));
    }

    @Test
    public void test_ItemNotFoundException_withParamThrowable() throws Exception {
        Assert.assertNotNull(new ItemNotFoundException(new Throwable("error")));

    }


}