package net.grandcentrix.tray.sample.provider;

import net.grandcentrix.tray.PersonDataAbility;
import net.grandcentrix.tray.Tray;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.core.Preferences;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.SqliteHelper;
import net.grandcentrix.tray.provider.TrayDBHelper;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.app.AbilityContext;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, Uri.class, TrayPreferences.class, Preferences.class, TrayLog.class, HiLog.class, Tray.class, HiLogLabel.class, PersonDataAbility.class, SqliteHelper.class, DataAbilityHelper.class, AbilityContext.class, SqliteHelper.class, TrayDBHelper.class})
public class TrayDBHelperTest {
    TrayDBHelper trayDBHelper;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);

        trayDBHelper = mock(TrayDBHelper.class);


    }

    @Test
    public void onOpen() {
    }

    @Test
    public void test_logTag() {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        try {
            whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            doCallRealMethod().when(trayDBHelper, "logTag");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Assert.assertNotNull(Whitebox.invokeMethod(trayDBHelper, "logTag"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test_onCreate() {
        RdbStore rdbStore = mock(RdbStore.class);
        doCallRealMethod().when(trayDBHelper).onCreate(any());
        trayDBHelper.onCreate(rdbStore);
        PowerMockito.verifyStatic(TrayLog.class, Mockito.atLeast(1));
        TrayLog.v(any());
    }

    @Test
    public void test_onUpgrade() {
        RdbStore rdbStore = mock(RdbStore.class);
        doCallRealMethod().when(trayDBHelper).onUpgrade(rdbStore, 1, 1);
        trayDBHelper.onUpgrade(rdbStore, 1, 1);

    }

    @Test(expected = IllegalStateException.class)
    public void test_onUpgradeCaseException() {
        RdbStore rdbStore = mock(RdbStore.class);
        doCallRealMethod().when(trayDBHelper).onUpgrade(rdbStore, 1, 3);
        trayDBHelper.onUpgrade(rdbStore, 1, 3);

    }

    @Test(expected = IllegalArgumentException.class)
    public void test_onUpgradeCaseException2() {
        RdbStore rdbStore = mock(RdbStore.class);
        doCallRealMethod().when(trayDBHelper).onUpgrade(rdbStore, 0, 2);
        trayDBHelper.onUpgrade(rdbStore, 0, 2);

    }

    @Test
    public void test_getStore() {
        doCallRealMethod().when(trayDBHelper).getStore();

        Assert.assertNull(trayDBHelper.getStore());
    }

    @Test
    public void test_createV1() {
        RdbStore rdbStore = mock(RdbStore.class);
        try {
            doCallRealMethod().when(trayDBHelper, "createV1", rdbStore);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Whitebox.invokeMethod(trayDBHelper, "createV1", rdbStore);
            Mockito.verify(rdbStore, Mockito.atLeastOnce()).executeSql(any());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_upgradeToV2() {
        RdbStore rdbStore = mock(RdbStore.class);
        try {
            doCallRealMethod().when(trayDBHelper, "upgradeToV2", rdbStore);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Whitebox.invokeMethod(trayDBHelper, "upgradeToV2", rdbStore);
            Mockito.verify(rdbStore, Mockito.atLeastOnce()).executeSql(any());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}