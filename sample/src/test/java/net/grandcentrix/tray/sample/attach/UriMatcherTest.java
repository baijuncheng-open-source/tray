package net.grandcentrix.tray.sample.attach;

import net.grandcentrix.tray.attach.UriMatcher;
import net.grandcentrix.tray.core.Preferences;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.TrayContract;
import ohos.agp.utils.TextTool;
import ohos.utils.net.Uri;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class})
public class UriMatcherTest {
    UriMatcher sURIMatcher;
    int finalCode;

    @Before
    public void setUp() throws Exception {
        sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test(expected = IllegalArgumentException.class)
    public void test_addURIForErrorCode() {
        sURIMatcher.addURI("http://", "path1", -1);
    }

    @Test
    public void test_addURIForNormalCode() {
        sURIMatcher.addURI("http://", "path1", 1);
    }

    @Test
    public void test_addURIForNormalPath() {
        sURIMatcher.addURI("http://", "//sms", 1);
        getInternalCode(sURIMatcher);
        Assert.assertEquals(1, finalCode);
    }

    public void getInternalCode(UriMatcher sURIMatcher) {
        try {
            Class c = sURIMatcher.getClass();
            Field field = c.getDeclaredField("mChildren");
            Field fieldCode = c.getDeclaredField("mCode");
            field.setAccessible(true);
            fieldCode.setAccessible(true);
            ArrayList<UriMatcher> arrayList = (ArrayList<UriMatcher>) field.get(sURIMatcher);
            if (arrayList.size() > 0) {
                getInternalCode(arrayList.get(0));
            } else {
                finalCode = fieldCode.getInt(sURIMatcher);
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_matchWithCodeError() {
        sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        sURIMatcher.addURI("net.grandcentrix.tray.PersonDataAbility",
                TrayContract.Preferences.BASE_PATH,
                30);
        String url = "dataability:///" + "net.grandcentrix.tray.PersonDataAbility";

        Uri uri = mock(Uri.class);
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.when(Uri.parse("uri")).thenReturn(uri);

        when(uri.getDecodedPathList()).thenReturn(new ArrayList<>());
        when(uri.getDecodedAuthority()).thenReturn(null);

        final int code = sURIMatcher.match(uri);
        Assert.assertEquals(-1, code);

    }

    @Test
    public void test_matchWithCodeNormal() {
        sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        sURIMatcher.addURI("net.grandcentrix.tray.PersonDataAbility",
                TrayContract.Preferences.BASE_PATH,
                30);
        String url = "dataability:///" + "net.grandcentrix.tray.PersonDataAbility";

        Uri uri = mock(Uri.class);
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.when(Uri.parse("uri")).thenReturn(uri);
        ArrayList arrayList = new ArrayList();
        arrayList.add("jp");
        arrayList.add("jm");
        arrayList.add("jz");
        arrayList.add("jc");
        when(uri.getDecodedPathList()).thenReturn(arrayList);
        when(uri.getDecodedAuthority()).thenReturn(null);

        final int code = sURIMatcher.match(uri);
        Assert.assertEquals(-1, code);

    }

    @Test
    public void test_matchWithCode_withNUMBER() {
        sURIMatcher.addURI("#",
                TrayContract.Preferences.BASE_PATH,
                30);
        String url = "dataability:///" + "net.grandcentrix.tray.PersonDataAbility";

        Uri uri = mock(Uri.class);
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.when(Uri.parse("uri")).thenReturn(uri);
        ArrayList arrayList = new ArrayList();
        arrayList.add("jp");
        arrayList.add("jm");
        arrayList.add("jz");
        arrayList.add("jc");
        when(uri.getDecodedPathList()).thenReturn(arrayList);
        when(uri.getDecodedAuthority()).thenReturn("456");

        final int code = sURIMatcher.match(uri);
        Assert.assertEquals(-1, code);

    }

    @Test
    public void test_matchWithCode_withText() {
        sURIMatcher.addURI("*",
                TrayContract.Preferences.BASE_PATH,
                30);
        String url = "dataability:///" + "net.grandcentrix.tray.PersonDataAbility";

        Uri uri = mock(Uri.class);
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.when(Uri.parse("uri")).thenReturn(uri);
        ArrayList arrayList = new ArrayList();
        arrayList.add("jp");
        arrayList.add("jm");
        arrayList.add("jz");
        arrayList.add("jc");
        when(uri.getDecodedPathList()).thenReturn(arrayList);
        when(uri.getDecodedAuthority()).thenReturn("456");

        final int code = sURIMatcher.match(uri);
        Assert.assertEquals(-1, code);

    }


    @Test
    public void test_convertUri() {
        String url = "dataability:///" + "net.grandcentrix.tray.PersonDataAbility";
        Uri uri = mock(Uri.class);
        when(uri.toString()).thenReturn(url);
        PowerMockito.mockStatic(Uri.class);
        UriMatcher.convertUri(uri);
        PowerMockito.verifyStatic(Uri.class);
        Uri.parse(Mockito.any());
    }

    @Test(expected = NullPointerException.class)
    public void test_convertUri_caseNull() {
        String url = "dataability:///" + "net.grandcentrix.tray.PersonDataAbility";
        Uri uri = mock(Uri.class);
        when(uri.toString()).thenReturn(url);
        PowerMockito.mockStatic(Uri.class);
        UriMatcher.convertUri(null);
        PowerMockito.verifyStatic(Uri.class);
        Uri.parse(Mockito.any());
    }

    @Test
    public void test_revertUri() {
        String url = "dataability://" + "net.grandcentrix.tray.PersonDataAbility";
        Uri uri = mock(Uri.class);
        when(uri.toString()).thenReturn(url);
        PowerMockito.mockStatic(Uri.class);
        UriMatcher.revertUri(uri);
        PowerMockito.verifyStatic(Uri.class);
        Uri.parse(Mockito.any());
    }

    @Test(expected = NullPointerException.class)
    public void test_revertUri_caseNull() {
        String url = "dataability://" + "net.grandcentrix.tray.PersonDataAbility";
        Uri uri = mock(Uri.class);
        when(uri.toString()).thenReturn(url);
        PowerMockito.mockStatic(Uri.class);
        UriMatcher.revertUri(null);
        PowerMockito.verifyStatic(Uri.class);
        Uri.parse(Mockito.any());
    }
}