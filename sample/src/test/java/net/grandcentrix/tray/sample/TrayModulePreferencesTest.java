package net.grandcentrix.tray.sample;

import net.grandcentrix.tray.PersonDataAbility;
import net.grandcentrix.tray.sample.og.ImportTrayPreferences;
import net.grandcentrix.tray.TrayModulePreferences;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.ContentProviderStorage;
import net.grandcentrix.tray.provider.TrayContract;
import net.grandcentrix.tray.provider.TrayProviderHelper;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, TrayProviderHelper.class, TrayLog.class, HiLog.class, TextTool.class, TrayContract.class, ContentProviderStorage.class, HiLogLabel.class, Log.class, EventRunner.class, DataAbilityHelper.class, ImportTrayPreferences.class, PersonDataAbility.class})
public class TrayModulePreferencesTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void test_constructor() throws Exception {
        ContentProviderStorage contentProviderStorage = mock(ContentProviderStorage.class);
        whenNew(ContentProviderStorage.class).withAnyArguments().thenReturn(contentProviderStorage);
        Context context = mock(Context.class);
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        Assert.assertNotNull(new TrayModulePreferences(context, "module", 1));

    }

    @Test
    public void test_constructor_caseNull() throws Exception {
        ContentProviderStorage contentProviderStorage = mock(ContentProviderStorage.class);
        whenNew(ContentProviderStorage.class).withAnyArguments().thenReturn(contentProviderStorage);
        Context context = mock(Context.class);
        PowerMockito.mockStatic(HiLog.class);
        PowerMockito.mockStatic(TrayLog.class);
        Assert.assertNotNull(new TrayModulePreferences(context, null, 1));

    }
}