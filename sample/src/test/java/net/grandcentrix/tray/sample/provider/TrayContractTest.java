package net.grandcentrix.tray.sample.provider;

import net.grandcentrix.tray.sample.og.ImportTrayPreferences;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.ContentProviderStorage;
import net.grandcentrix.tray.provider.SqliteHelper;
import net.grandcentrix.tray.provider.TrayContract;
import net.grandcentrix.tray.provider.TrayProviderHelper;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.rdb.RawRdbPredicates;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, TrayProviderHelper.class, TrayLog.class, HiLog.class, TextTool.class, TrayContract.class, ContentProviderStorage.class, HiLogLabel.class, Log.class, EventRunner.class, DataAbilityHelper.class, ImportTrayPreferences.class, RawRdbPredicates.class, SqliteHelper.class})
public class TrayContractTest {
    TrayContract trayContract;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(Uri.class);
        Uri uri = mock(Uri.class);
        when(Uri.parse(any())).thenReturn(uri);
        trayContract = new TrayContract();
    }

    @Test
    public void test_generateContentUri() {
        Context context = mock(Context.class);
        TrayContract.generateContentUri(context);
        PowerMockito.verifyStatic(Uri.class);
        Uri.appendEncodedPathToUri(any(), any());

    }

    @Test
    public void test_generateContentUri_caseNull() {
        TrayContract.generateContentUri(null);
        PowerMockito.verifyStatic(Uri.class);
        Uri.appendEncodedPathToUri(any(), any());

    }

}