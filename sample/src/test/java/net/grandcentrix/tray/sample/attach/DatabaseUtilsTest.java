package net.grandcentrix.tray.sample.attach;

import net.grandcentrix.tray.attach.DatabaseUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class DatabaseUtilsTest {
    StringBuilder builder;

    @Before
    public void setUp() throws Exception {
        builder = new StringBuilder();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test_sqlEscapeString() {
        String result = DatabaseUtils.sqlEscapeString("sms\'stdu");
        Assert.assertTrue(result.startsWith("\'") && result.endsWith("\'"));
    }

    @Test
    public void test_appendEscapedSQLString_case_withPointIn() {
        builder.setLength(0);
        DatabaseUtils.appendEscapedSQLString(builder, "sms\'stdu");
        int count = builder.toString().split("\'").length;
        Assert.assertTrue(count == 4);
    }

    @Test
    public void test_appendEscapedSQLString_case_withoutPointIn() {
        builder.setLength(0);
        DatabaseUtils.appendEscapedSQLString(builder, "smsstdu");
        int count = builder.toString().split("\'").length;
        Assert.assertTrue(count == 2);

    }
}