package net.grandcentrix.tray.sample.provider;

import net.grandcentrix.tray.sample.og.ImportTrayPreferences;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.core.*;
import net.grandcentrix.tray.provider.ContentProviderStorage;
import net.grandcentrix.tray.provider.TrayContract;
import net.grandcentrix.tray.provider.TrayProviderHelper;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, TrayProviderHelper.class, TrayLog.class, HiLog.class, TextTool.class, TrayContract.class, ContentProviderStorage.class, HiLogLabel.class, Log.class, EventRunner.class, DataAbilityHelper.class, ImportTrayPreferences.class})
public class ContentProviderStorageTest {
    ContentProviderStorage contentProviderStorage;
    TrayProviderHelper trayProviderHelper;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(HiLog.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        PowerMockito.mockStatic(HiLogLabel.class);
        PowerMockito.mockStatic(Log.class);
        PowerMockito.mockStatic(TrayLog.class);
        Context context = mock(Context.class);
        when(context.getApplicationContext()).thenReturn(context);
        PowerMockito.mockStatic(Uri.class);
        Uri uri = mock(Uri.class);
        Uri.Builder builder = mock(Uri.Builder.class);
        when(uri.makeBuilder()).thenReturn(builder);
        when(builder.build()).thenReturn(uri);


        when(Uri.parse(any())).thenReturn(uri);

        PowerMockito.mockStatic(TrayContract.class);
        when(TrayContract.generateContentUri(any())).thenReturn(uri);
        when(TrayContract.class, "generateInternalContentUri", any()).thenReturn(uri);
        when(uri.makeBuilder()).thenReturn(builder);


        trayProviderHelper = mock(TrayProviderHelper.class);
        whenNew(TrayProviderHelper.class).withAnyArguments().thenReturn(trayProviderHelper);


        when(trayProviderHelper.remove(any())).thenReturn(true);


        contentProviderStorage = new ContentProviderStorage(context, "module1", TrayStorage.Type.USER);

    }

    @Test
    public void test_getModuleName() {
        Assert.assertEquals("module1", contentProviderStorage.getModuleName());
    }

    @Test
    public void test_getType() {
        Assert.assertEquals(TrayStorage.Type.USER, contentProviderStorage.getType());
    }

    @Test
    public void test_annex() {
        contentProviderStorage.annex(contentProviderStorage);
    }

    @Test
    public void test_clear() {
        Assert.assertTrue(contentProviderStorage.clear());
    }

    @Test
    public void test_get() {
        Assert.assertNull(contentProviderStorage.get("module1"));
    }

    @Test
    public void test_getWithCase() {
        List list = new ArrayList();
        list.add(new TrayItem("module", "key", "migratedKey", "value", new Date(), new Date()));
        list.add(new TrayItem("module", "key", "migratedKey", "value", new Date(), new Date()));

        when(trayProviderHelper.queryProviderSafe(any(Uri.class))).thenReturn(list);

        Assert.assertNotNull(contentProviderStorage.get("module1"));
    }

    @Test
    public void test_getWith_caseNull() {
        List list = new ArrayList();
        list.add(new TrayItem("module", "key", "migratedKey", "value", new Date(), new Date()));
        list.add(new TrayItem("module", "key", "migratedKey", "value", new Date(), new Date()));

        when(trayProviderHelper.queryProviderSafe(any(Uri.class))).thenReturn(list);

        Assert.assertNotNull(contentProviderStorage.get(null));
    }

    @Test
    public void test_getAll() {
        Assert.assertNotNull(contentProviderStorage.getAll());
    }

    @Test
    public void test_getContext() {
        Assert.assertNotNull(contentProviderStorage.getContext());
    }

    @Test
    public void test_getVersion() {
        try {
            Assert.assertTrue(contentProviderStorage.getVersion() == 0);
        } catch (TrayException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test_registerOnTrayPreferenceChangeListener() {

        PowerMockito.mockStatic(EventRunner.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        doNothing().when(dataAbilityHelper).registerObserver(any(), any());

        EventRunner eventRunner = mock(EventRunner.class);
        EventRunner eventRunner1 = mock(EventRunner.class);
        when(EventRunner.getMainEventRunner()).thenReturn(eventRunner);
        when(EventRunner.create()).thenReturn(eventRunner1);
        EventHandler eventHandler = mock(EventHandler.class);
        try {
            whenNew(EventHandler.class).withAnyArguments().thenReturn(eventHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Runnable runnable = invocationOnMock.getArgument(0);
                runnable.run();
                return null;
            }
        }).when(eventHandler).postTask(any());


        contentProviderStorage.registerOnTrayPreferenceChangeListener(new OnTrayPreferenceChangeListener() {
            @Override
            public void onTrayPreferenceChanged(Collection<TrayItem> items) {

            }
        });
        PowerMockito.verifyStatic(DataAbilityHelper.class);
        DataAbilityHelper.creator(any());

    }

    @Test(expected = IllegalArgumentException.class)
    public void test_remove() {
        contentProviderStorage.remove(null);
    }

    @Test
    public void test_removeNormal() {
        contentProviderStorage.remove("key");
        Mockito.verify(trayProviderHelper, Mockito.atLeastOnce()).removeAndCount(any());
    }

    @Test
    public void test_setVersion() {
        contentProviderStorage.setVersion(1);
        Mockito.verify(trayProviderHelper, Mockito.atLeastOnce()).persist(any(), any());
    }

    @Test
    public void test_unregisterOnTrayPreferenceChangeListener() {
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        doNothing().when(dataAbilityHelper).unregisterObserver(any(), any());

        contentProviderStorage.unregisterOnTrayPreferenceChangeListener(new OnTrayPreferenceChangeListener() {
            @Override
            public void onTrayPreferenceChanged(Collection<TrayItem> items) {

            }
        });
        Mockito.verify(dataAbilityHelper, Mockito.atLeastOnce()).unregisterObserver(any(), any());


    }

    @Test
    public void test_wipe() {
        contentProviderStorage.wipe();
        Mockito.verify(trayProviderHelper, Mockito.atLeastOnce()).remove(any());
    }

    @Test
    public void test_put() {
        Assert.assertFalse(contentProviderStorage.put(new TrayItem("module", "key", "migratedKey", "value", new Date(), new Date())));
    }

    @Test
    public void test_putImport() {
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ImportTrayPreferences importTrayPreferences = mock(ImportTrayPreferences.class);
        contentProviderStorage.put("key", "migrationKey", importTrayPreferences);
        Mockito.verify(trayProviderHelper, Mockito.atLeastOnce()).persist(any(Uri.class), anyString(), anyString());

    }

    @Test
    public void test_onChange() {
        PowerMockito.mockStatic(EventRunner.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        DataAbilityHelper dataAbilityHelper = mock(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any())).thenReturn(dataAbilityHelper);
        doNothing().when(dataAbilityHelper).registerObserver(any(), any());

        EventRunner eventRunner = mock(EventRunner.class);
        EventRunner eventRunner1 = mock(EventRunner.class);
        when(EventRunner.getMainEventRunner()).thenReturn(eventRunner);
        when(EventRunner.create()).thenReturn(eventRunner1);
        EventHandler eventHandler = mock(EventHandler.class);
        try {
            whenNew(EventHandler.class).withAnyArguments().thenReturn(eventHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Runnable runnable = invocationOnMock.getArgument(0);
                runnable.run();
                Class c = contentProviderStorage.getClass();
                Field field = c.getDeclaredField("mObserver");
                field.setAccessible(true);
                Method method = field.getType().getDeclaredMethod("onChange");
                method.setAccessible(true);
                method.invoke(field.get(contentProviderStorage));
                Mockito.verify(eventHandler, Mockito.atLeastOnce()).postSyncTask(any());

                return null;
            }
        }).when(eventHandler).postTask(any());
        contentProviderStorage.registerOnTrayPreferenceChangeListener(new OnTrayPreferenceChangeListener() {
            @Override
            public void onTrayPreferenceChanged(Collection<TrayItem> items) {

            }
        });


    }
}