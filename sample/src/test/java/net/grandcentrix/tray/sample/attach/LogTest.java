package net.grandcentrix.tray.sample.attach;

import net.grandcentrix.tray.PersonDataAbility;
import net.grandcentrix.tray.sample.og.ImportTrayPreferences;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.ContentProviderStorage;
import net.grandcentrix.tray.provider.TrayContract;
import net.grandcentrix.tray.provider.TrayProviderHelper;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.utils.TextTool;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, TrayProviderHelper.class, TrayLog.class, HiLog.class, TextTool.class, TrayContract.class, ContentProviderStorage.class, HiLogLabel.class, Log.class, EventRunner.class, DataAbilityHelper.class, ImportTrayPreferences.class, PersonDataAbility.class})
public class LogTest {

    @Before
    public void setUp() throws Exception {
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
    }

    @Test
    public void test_v() {
        Assert.assertEquals(0, Log.v("tag", "please input "));
    }

    @Test
    public void test_d() {
        Assert.assertEquals(0, Log.d("tag", "please input "));
    }

    @Test
    public void test_i() {
        Assert.assertEquals(0, Log.i("tag", "please input "));
    }

    @Test
    public void test_w() {
        Assert.assertEquals(0, Log.w("tag", "please input "));
    }

    @Test
    public void test_W() {
        Assert.assertEquals(0, Log.w("tag", "please input ", new Throwable("code1")));
    }

    @Test
    public void test_e() {
        Assert.assertEquals(0, Log.e("tag", "please input ", new Throwable("code1")));
    }

    @Test
    public void test_E() {
        Assert.assertEquals(0, Log.e("tag", "please input "));
    }

}