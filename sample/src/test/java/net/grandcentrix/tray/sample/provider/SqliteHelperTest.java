package net.grandcentrix.tray.sample.provider;

import net.grandcentrix.tray.sample.og.ImportTrayPreferences;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.core.TrayLog;
import net.grandcentrix.tray.provider.ContentProviderStorage;
import net.grandcentrix.tray.provider.SqliteHelper;
import net.grandcentrix.tray.provider.TrayContract;
import net.grandcentrix.tray.provider.TrayProviderHelper;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.utils.TextTool;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, TrayPreferences.class, TrayProviderHelper.class, TrayLog.class, HiLog.class, TextTool.class, TrayContract.class, ContentProviderStorage.class, HiLogLabel.class, Log.class, EventRunner.class, DataAbilityHelper.class, ImportTrayPreferences.class, RawRdbPredicates.class, SqliteHelper.class})
public class SqliteHelperTest {

    @Before
    public void setUp() throws Exception {
//        PowerMockito.mockStatic(SqliteHelper.class);
        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
    }

    @Test
    public void test_extendSelection() {
        PowerMockito.mockStatic(TextTool.class);
        PowerMockito.when(TextTool.isNullOrEmpty(any())).thenReturn(false);
        Assert.assertTrue(SqliteHelper.extendSelection("selection", "add").contains("add"));
    }

    @Test
    public void test_extendSelection_caseNull() {
        PowerMockito.mockStatic(TextTool.class);
        PowerMockito.when(TextTool.isNullOrEmpty(any())).thenReturn(false);
        Assert.assertFalse(SqliteHelper.extendSelection(null, null).contains("add"));
    }

    @Test
    public void test_extendSelectionArgs() {
        Assert.assertTrue(SqliteHelper.extendSelectionArgs(new String[]{"a", "b"}, new String[]{"a", "b"}).length == 4);

    }


    @Test
    public void test_ExtendSelectionArgs_params2() {
        PowerMockito.mockStatic(TextTool.class);
        PowerMockito.when(TextTool.isNullOrEmpty(any())).thenReturn(false);
        Assert.assertTrue(SqliteHelper.extendSelectionArgs("a", new String[]{"a", "b"}).length == 3);

    }



    @Test
    public void test_insertOrUpdate() {
        ValuesBucket valuesBucket = mock(ValuesBucket.class);
        Assert.assertTrue(SqliteHelper.insertOrUpdate(null, "table1", "selection", new String[]{"cab"}, valuesBucket, new String[]{"exclude"}) == -1);

    }

    @Test
    public void test_insertOrUpdate_CaseRow0() throws Exception {
        RdbStore rdbStore = mock(RdbStore.class);
        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
        ValuesBucket valuesBucket = mock(ValuesBucket.class);

        ResultSet resultSet = mock(ResultSet.class);
        when(rdbStore.query(any(), any())).thenReturn(resultSet);
        when(resultSet.getRowCount()).thenReturn(0);
        when(rdbStore.insert(any(), any())).thenReturn(-1l);

        Assert.assertTrue(SqliteHelper.insertOrUpdate(rdbStore, "table1", "selection", new String[]{"cab"}, valuesBucket, new String[]{"exclude"}) == -1);

    }

    @Test
    public void test_insertOrUpdate_caseInsert0() throws Exception {
        RdbStore rdbStore = mock(RdbStore.class);
        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
        ValuesBucket valuesBucket = mock(ValuesBucket.class);

        ResultSet resultSet = mock(ResultSet.class);
        when(rdbStore.query(any(), any())).thenReturn(resultSet);
        when(resultSet.getRowCount()).thenReturn(0);
        when(rdbStore.insert(any(), any())).thenReturn(0l);

        Assert.assertTrue(SqliteHelper.insertOrUpdate(rdbStore, "table1", "selection", new String[]{"cab"}, valuesBucket, new String[]{"exclude"}) == 1);

    }

    @Test
    public void test_insertOrUpdate_caseItemsPositive() throws Exception {
        RdbStore rdbStore = mock(RdbStore.class);
        RawRdbPredicates rawRdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rawRdbPredicates);
        ValuesBucket valuesBucket = mock(ValuesBucket.class);

        ResultSet resultSet = mock(ResultSet.class);
        when(rdbStore.query(any(), any())).thenReturn(resultSet);
        when(resultSet.getRowCount()).thenReturn(1);
        when(rdbStore.insert(any(), any())).thenReturn(0l);

        Assert.assertTrue(SqliteHelper.insertOrUpdate(rdbStore, "table1", "selection", new String[]{"cab"}, valuesBucket, new String[]{"exclude"}) == 0);

    }
}