package net.grandcentrix.tray.sample.core;

import net.grandcentrix.tray.core.OnTrayPreferenceChangeListener;
import net.grandcentrix.tray.core.TrayException;
import net.grandcentrix.tray.core.TrayItem;
import net.grandcentrix.tray.core.TrayStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;


public class TrayStorageTest {
    TrayStorage trayStorage;

    @Before
    public void setUp() throws Exception {
        trayStorage = new TrayStorage("mModuleName", TrayStorage.Type.USER) {
            @Override
            public void annex(TrayStorage oldStorage) {

            }

            @Override
            public void registerOnTrayPreferenceChangeListener(OnTrayPreferenceChangeListener listener) {

            }

            @Override
            public void unregisterOnTrayPreferenceChangeListener(OnTrayPreferenceChangeListener listener) {

            }

            @Override
            public boolean clear() {
                return false;
            }

            @Override
            public TrayItem get(String key) {
                return null;
            }

            @Override
            public Collection<TrayItem> getAll() {
                return null;
            }

            @Override
            public int getVersion() throws TrayException {
                return 0;
            }

            @Override
            public boolean put(TrayItem item) {
                return false;
            }

            @Override
            public boolean put(String key, String migrationKey, Object data) {
                return false;
            }

            @Override
            public boolean put(String key, Object data) {
                return false;
            }

            @Override
            public boolean remove(String key) {
                return false;
            }

            @Override
            public boolean setVersion(int version) {
                return false;
            }

            @Override
            public boolean wipe() {
                return false;
            }
        };
    }

    @Test
    public void test_getModuleName() {
        Assert.assertEquals("mModuleName", trayStorage.getModuleName());
    }

    @Test
    public void test_getType() {
        Assert.assertEquals(TrayStorage.Type.USER, trayStorage.getType());
    }

}