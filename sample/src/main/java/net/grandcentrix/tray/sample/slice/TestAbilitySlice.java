/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.grandcentrix.tray.sample.slice;

import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.sample.MainAbility;
import net.grandcentrix.tray.sample.ResourceTable;
import net.grandcentrix.tray.sample.og.ImportTrayPreferences;
import net.grandcentrix.tray.core.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.TextTool;
import ohos.agp.window.dialog.ToastDialog;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import rx.Observable;
import rx.functions.Func1;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class TestAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "Demo");
    public static final String STARTUP_COUNT = "startup_count";

    public static final String SHARED_PREF_NAME = "shared_pref";

    private static final String SHARED_PREF_KEY = "shared_pref_key";

    private static final String TRAY_PREF_KEY = "importedData";

    private static final String TAG = MainAbilitySlice.class.getSimpleName();

    private ImportTrayPreferences mAppPrefs;
    private Preferences mSharedPreferences;
    private TextField testInputKey;
    private TextField testInputValue;
    private TextField testQueryKey;
    private TextField migrateKey;
    private TextField migrateValue;
    private Text testQueryResult;
    private int index = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_layout_test);
        HiLog.warn(LABEL, "Failed to visit %{private}s.", "init");
        ini();
        mSharedPreferences.registerObserver(mSharedPrefsListener);
        mImportPreference.registerOnTrayPreferenceChangeListener(mImportPrefsListener);
        mAppPrefs.registerOnTrayPreferenceChangeListener(mAppPrefsListener);
    }

    private void ini() {
        mAppPrefs = new ImportTrayPreferences(this);
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext()); // context入参类型为ohos.app.Context。
//      String fileName = "name"; // fileName表示文件名，其取值不能为空，也不能包含路径，默认存储目录可以通过context.getPreferencesDir()获取。
        mSharedPreferences = databaseHelper.getPreferences(MainAbility.SHARED_PREF_NAME);

        int startupCount = mAppPrefs.getInt(STARTUP_COUNT, 0);

        if (startupCount == 0) {
            mSharedPreferences
                    .putString("userToken", UUID.randomUUID().toString())
                    .putString("gcmToken", UUID.randomUUID().toString())
                    .flush();
        }
        mAppPrefs.put(STARTUP_COUNT, ++startupCount);

        testAutoBackup();

        mImportPreference = new ImportTrayPreferences(this);

        testInputKey = (TextField) findComponentById(ResourceTable.Id_test_input_key);
        testInputValue = (TextField) findComponentById(ResourceTable.Id_test_input_value);
//
        final Button testInsert = (Button) findComponentById(ResourceTable.Id_test_insert);
        testInsert.setClickedListener(this);
        final Button testContained = (Button) findComponentById(ResourceTable.Id_test_contained);
        testContained.setClickedListener(this);
        final Button testRemoved = (Button) findComponentById(ResourceTable.Id_test_removed);
        testRemoved.setClickedListener(this);
        final Button testWipe = (Button) findComponentById(ResourceTable.Id_test_wipe);
        testWipe.setClickedListener(this);
        final Button testClear = (Button) findComponentById(ResourceTable.Id_test_clear);
        testClear.setClickedListener(this);
        final Button testMigrate = (Button) findComponentById(ResourceTable.Id_test_migrate);
        testMigrate.setClickedListener(this);

        testQueryKey = (TextField) findComponentById(ResourceTable.Id_test_query_key_input);
        migrateKey = (TextField) findComponentById(ResourceTable.Id_migrate_input_key);
        migrateValue = (TextField) findComponentById(ResourceTable.Id_migrate_input_value);
        Button migrateBt = (Button) findComponentById(ResourceTable.Id_migrate_insert);
        migrateBt.setClickedListener(this);
        final Button testQuery = (Button) findComponentById(ResourceTable.Id_test_query_execute);
        testQuery.setClickedListener(this);

        testQueryResult = (Text) findComponentById(ResourceTable.Id_query_result);
        Text textCurrentType = (Text) findComponentById(ResourceTable.Id_current_type);


        RadioContainer container = (RadioContainer) findComponentById(ResourceTable.Id_radio_container);
        container.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int index) {
                TestAbilitySlice.this.index = index;
                String currentDes = "当前选择操作数据类型";
                switch (index) {
                    case 0:
                        currentDes += "String";
                        break;
                    case 1:
                        currentDes += "Int";
                        break;
                    case 2:
                        currentDes += "Long";
                        break;
                    case 3:
                        currentDes += "Float";
                        break;
                    case 4:
                        currentDes += "boolean";
                        break;
                }
                textCurrentType.setText(currentDes);
            }
        });


    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    private void testAutoBackup() {

        {
            // device specific data
            final TrayPreferences deviceSpecificPref =
                    new TrayPreferences(this, "nobackup", 1, TrayStorage.Type.DEVICE);
            final String deviceId = deviceSpecificPref.getString("deviceId", null);
//          Log.v(TAG, "deviceId: " + deviceId);
            if (deviceId == null) {
                final String uuid = UUID.randomUUID().toString();
                deviceSpecificPref.put("deviceId", uuid);
//              Log.v(TAG, "no deviceId, created: " + uuid);
            }
        }

        {
            // user specific data
            final TrayPreferences userSpecificPref =
                    new TrayPreferences(this, "autobackup", 1, TrayStorage.Type.USER);
            final String userId = userSpecificPref.getString("userId", null);
//          Log.v(TAG, "userId: " + userId);
            if (userId == null) {
                final String uuid = UUID.randomUUID().toString();
                userSpecificPref.put("userId", uuid);
//              Log.v(TAG, "no userId, created: " + uuid);
            }
        }
    }


    private ImportTrayPreferences mImportPreference;

    private int mMultiProcessCounter = 0;

    private OnTrayPreferenceChangeListener mAppPrefsListener
            = new OnTrayPreferenceChangeListener() {
        @Override
        public void onTrayPreferenceChanged(final Collection<TrayItem> items) {
            Log.d(TAG, "read in main process: changed " + getNiceString(items));
        }
    };

    private final OnTrayPreferenceChangeListener mImportPrefsListener
            = new OnTrayPreferenceChangeListener() {
        @Override
        public void onTrayPreferenceChanged(final Collection<TrayItem> items) {
//          Log.v(TAG, "trayPrefs changed items: " + getNiceString(items));
//            updateSharedPrefInfo();
        }
    };

    private final Preferences.PreferencesObserver mSharedPrefsListener
            = new Preferences.PreferencesObserver() {
        @Override
        public void onChange(Preferences preferences, String key) {
            Object value = null;
            try {
                value = mSharedPreferences.getString(key, "");
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
            try {
                value = mSharedPreferences.getInt(key, 0);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
//          Log.d(TAG, "sharedPrefs changed key: '" + key + "' value '" + value + "'");
//          updateSharedPrefInfo();
        }
    };

    private void updateSharedPrefInfo() {
        final Text info = (Text) findComponentById(ResourceTable.Id_shared_pref_info);
        final String sharedPrefData = mSharedPreferences.getString(SHARED_PREF_KEY, "null");
        final String trayData = mImportPreference.getString(TRAY_PREF_KEY, "null");

        info.setText("SharedPref Data: " + sharedPrefData + "\n"
                + "Tray Data: " + trayData);
    }

    private List<String> getNiceString(final Collection<TrayItem> items) {
        return Observable.from(items)
                .map(new Func1<TrayItem, String>() {
                    @Override
                    public String call(final TrayItem trayItem) {
                        return "key: '" + trayItem.key() + "' value '" + trayItem.value() + "'";
                    }
                })
                .toList().toBlocking().first();
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_test_insert:
                if (TextTool.isNullOrEmpty(testInputKey.getText()) || TextTool.isNullOrEmpty(testInputValue.getText())) {
                    new ToastDialog(getContext())
                            .setText("请检查输入值是否为空")
                            .show();
                    return;
                }
                boolean insertResult = false;
                try {
                    switch (index) {
                        case 0:
                            insertResult = mAppPrefs.put(testInputKey.getText(), testInputValue.getText());
                            break;
                        case 1:
                            insertResult = mAppPrefs.put(testInputKey.getText(), Integer.parseInt(testInputValue.getText()));
                            break;
                        case 2:
                            insertResult = mAppPrefs.put(testInputKey.getText(), Long.parseLong(testInputValue.getText()));
                            break;
                        case 3:
                            insertResult = mAppPrefs.put(testInputKey.getText(), Float.parseFloat(testInputValue.getText()));
                            break;
                        case 4:
                            boolean insertBoolean = false;
                            if (testInputValue.getText().equalsIgnoreCase("true")) {
                                insertBoolean = true;
                            } else if (testInputValue.getText().equalsIgnoreCase("false")) {
                                insertBoolean = false;
                            } else {
                                new ToastDialog(getContext()).setText("请输入true or false").show();
                                return;
                            }

                            insertResult = mAppPrefs.put(testInputKey.getText(), insertBoolean);
                            break;
                    }
                    new ToastDialog(getContext()).setText(insertResult ? "插入成功" : "插入失败").show();
                } catch (NumberFormatException e) {
                    new ToastDialog(getContext()).setText("请检查输入类型是否正确").show();
                }
//                mAppPrefs.put(testInputKey.getText(), testInputValue.getText());

                break;
            case ResourceTable.Id_test_query_execute:
                if (TextTool.isNullOrEmpty(testQueryKey.getText())) {
                    new ToastDialog(getContext())
                            .setText("请检查查询键值是否为空")
                            .show();
                    return;
                }

                try {
                    switch (index) {
                        case 0:
                            testQueryResult.setText(mAppPrefs.getString(testQueryKey.getText(), "default value show "));
                            break;
                        case 1:
                            String resultInt = "";
                            int getInt = mAppPrefs.getInt(testQueryKey.getText(), -1);
                            resultInt = getInt == -1 ? "default Int value -1" : resultInt + getInt;
                            testQueryResult.setText(resultInt);
                            break;
                        case 2:
                            String resultLong = "";
                            Long getLong = mAppPrefs.getLong(testQueryKey.getText(), -1);
                            resultLong = getLong == -1 ? "default Long value -1" : resultLong + getLong;
                            testQueryResult.setText(resultLong);
                            break;
                        case 3:
                            String resultFloat = "";
                            Float getFloat = mAppPrefs.getFloat(testQueryKey.getText(), -1);
                            resultFloat = getFloat == -1 ? "default Float value -1" : resultFloat + getFloat;
                            testQueryResult.setText(resultFloat);
                            break;
                        case 4:
                            testQueryResult.setText("" + mAppPrefs.getBoolean(testQueryKey.getText(), false));
                            break;
                    }
                } catch (Exception e) {
                    new ToastDialog(getContext()).setText("请切换String再尝试").show();
                }
                break;
            case ResourceTable.Id_test_contained:
                if (TextTool.isNullOrEmpty(testInputKey.getText())) {
                    return;
                }
                new ToastDialog(getContext()).setText(mAppPrefs.contains(testInputKey.getText()) ? "已存在" : "未包含").show();
                break;
            case ResourceTable.Id_test_removed:
                if (TextTool.isNullOrEmpty(testInputKey.getText())) {
                    return;
                }
                if (mAppPrefs.contains(testInputKey.getText())) {
                    boolean result = mAppPrefs.remove(testInputKey.getText());
                    new ToastDialog(getContext()).setText(result ? "移除成功" : "移除失败").show();
                } else {
                    new ToastDialog(getContext()).setText("未包含").show();
                }
                break;
            case ResourceTable.Id_test_wipe:
                if (mAppPrefs.wipe()) {
                    try {
                        new ToastDialog(getContext()).setText("success versionCode: " + mAppPrefs.getVersion()).show();
                    } catch (TrayException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case ResourceTable.Id_test_clear:
                if (mAppPrefs.clear()) {
                    try {
                        new ToastDialog(getContext()).setText("success versionCode: " + mAppPrefs.getVersion()).show();
                    } catch (TrayException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case ResourceTable.Id_test_migrate:
                //
                boolean result = importSharedPref();
                if (result) {
                    new ToastDialog(getContext()).setText("迁移成功").show();
                }
                break;
            case ResourceTable.Id_migrate_insert:
                //
                if (TextTool.isNullOrEmpty(migrateKey.getText()) || TextTool.isNullOrEmpty(migrateValue.getText())) {
                    new ToastDialog(getContext()).setText("请检查输入值是否为空").show();
                    return;
                }

                try {
                    switch (index) {
                        case 0:
                            mSharedPreferences.putString(migrateKey.getText(), migrateValue.getText()).flush();
                            break;
                        case 1:
                            mSharedPreferences.putInt(migrateKey.getText(), Integer.parseInt(migrateValue.getText())).flush();
                            break;
                        case 2:
                            mSharedPreferences.putLong(migrateKey.getText(), Long.parseLong(migrateValue.getText())).flush();
                            break;
                        case 3:
                            mSharedPreferences.putFloat(migrateKey.getText(), Float.parseFloat(migrateValue.getText())).flush();
                            break;
                        case 4:
                            boolean insertBoolean = false;
                            if (migrateValue.getText().equalsIgnoreCase("true")) {
                                insertBoolean = true;
                            } else if (migrateValue.getText().equalsIgnoreCase("false")) {
                                insertBoolean = false;
                            } else {
                                new ToastDialog(getContext()).setText("请输入true or false").show();
                                return;
                            }

                            mSharedPreferences.putBoolean(migrateKey.getText(), insertBoolean).flush();
                            break;
                    }
                    new ToastDialog(getContext()).setText("插入成功").show();
                } catch (NumberFormatException e) {
                    new ToastDialog(getContext()).setText("请检查输入类型是否正确").show();
                }

                break;
            default:
                break;
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        mSharedPreferences.unregisterObserver(mSharedPrefsListener);
        mImportPreference.unregisterOnTrayPreferenceChangeListener(mImportPrefsListener);
        mAppPrefs.unregisterOnTrayPreferenceChangeListener(mAppPrefsListener);
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        terminateAbility();
    }

    private boolean importSharedPref() {
        final SharedPreferencesImport sharedPreferencesImport =
                new SharedPreferencesImport(this, MainAbility.SHARED_PREF_NAME,
                        migrateKey.getText(), migrateKey.getText());

        try {
            mImportPreference.migrate(sharedPreferencesImport);
        } catch (Exception e) {
            e.printStackTrace();
            new ToastDialog(getContext()).setText("无有效键值，请先插入").show();
            return false;
        }
        return true;
    }

}
