package net.grandcentrix.tray.sample.slice;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.Tray;
import net.grandcentrix.tray.TrayPreferences;
import net.grandcentrix.tray.attach.Log;
import net.grandcentrix.tray.core.OnTrayPreferenceChangeListener;
import net.grandcentrix.tray.core.SharedPreferencesImport;
import net.grandcentrix.tray.core.TrayItem;
import net.grandcentrix.tray.core.TrayStorage;
import net.grandcentrix.tray.sample.MainAbility;
import net.grandcentrix.tray.sample.ProcessServiceAbility;
import net.grandcentrix.tray.sample.ResourceTable;
import net.grandcentrix.tray.sample.og.ImportTrayPreferences;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import rx.Observable;
import rx.functions.Func1;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "Demo");
    public static final String STARTUP_COUNT = "startup_count";

    public static final String SHARED_PREF_NAME = "shared_pref";

    private static final String SHARED_PREF_KEY = "shared_pref_key";

    private static final String TRAY_PREF_KEY = "importedData";

    private static final String TAG = MainAbilitySlice.class.getSimpleName();

    private AppPreferences mAppPrefs;
    private Preferences mSharedPreferences;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_layout_cv);
        HiLog.warn(LABEL, "Failed to visit %{private}s.", "init");
        ini();
        mSharedPreferences.registerObserver(mSharedPrefsListener);
        mImportPreference.registerOnTrayPreferenceChangeListener(mImportPrefsListener);
        mAppPrefs.registerOnTrayPreferenceChangeListener(mAppPrefsListener);
    }

    private void ini() {
        mAppPrefs = new AppPreferences(this);
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext()); // context入参类型为ohos.app.Context。
//      String fileName = "name"; // fileName表示文件名，其取值不能为空，也不能包含路径，默认存储目录可以通过context.getPreferencesDir()获取。
        mSharedPreferences = databaseHelper.getPreferences(MainAbility.SHARED_PREF_NAME);

        int startupCount = mAppPrefs.getInt(STARTUP_COUNT, 0);

        if (startupCount == 0) {
            // save some "old" preferences which get migrated into the ImportTrayPreferences.
            // this works only the very first time ImportTrayPreferences gets created. You need to
            // wipe (clean is not enough) ImportTrayPreferences or delete the app data to retrigger
            // the call to ImportTrayPreferences#onCreate()
            mSharedPreferences
                    .putString("userToken", UUID.randomUUID().toString())
                    .putString("gcmToken", UUID.randomUUID().toString())
                    .flush();
        }

        mAppPrefs.put(STARTUP_COUNT, ++startupCount);

        testAutoBackup();

        mImportPreference = new ImportTrayPreferences(this);

        final Text text = (Text) findComponentById(ResourceTable.Id_text1);
        text.setText("count now:" + startupCount);

        final Button resetBtn = (Button) findComponentById(ResourceTable.Id_reset);
        resetBtn.setClickedListener(this);

        final Button writeSharedPref = (Button) findComponentById(ResourceTable.Id_write_shared_pref);
        writeSharedPref.setClickedListener(this);

        final Button importInTray = (Button) findComponentById(ResourceTable.Id_import_shared_pref);
        importInTray.setClickedListener(this);

        final Button multiprocessIncreaser = (Button) findComponentById(
                ResourceTable.Id_increase_multiprocess_counter);
        multiprocessIncreaser.setClickedListener(this);
        final Button multiprocessIncreaserOther = (Button) findComponentById(
                ResourceTable.Id_increase_multiprocess_counter_other_process);
        multiprocessIncreaserOther.setClickedListener(this);


    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    private void testAutoBackup() {

        {
            // device specific data
            final TrayPreferences deviceSpecificPref =
                    new TrayPreferences(this, "nobackup", 1, TrayStorage.Type.DEVICE);
            final String deviceId = deviceSpecificPref.getString("deviceId", null);
//          Log.v(TAG, "deviceId: " + deviceId);
            if (deviceId == null) {
                final String uuid = UUID.randomUUID().toString();
                deviceSpecificPref.put("deviceId", uuid);
//              Log.v(TAG, "no deviceId, created: " + uuid);
            }
        }

        {
            // user specific data
            final TrayPreferences userSpecificPref =
                    new TrayPreferences(this, "autobackup", 1, TrayStorage.Type.USER);
            final String userId = userSpecificPref.getString("userId", null);
//          Log.v(TAG, "userId: " + userId);
            if (userId == null) {
                final String uuid = UUID.randomUUID().toString();
                userSpecificPref.put("userId", uuid);
//              Log.v(TAG, "no userId, created: " + uuid);
            }
        }
    }


    private ImportTrayPreferences mImportPreference;

    private int mMultiProcessCounter = 0;

    private OnTrayPreferenceChangeListener mAppPrefsListener
            = new OnTrayPreferenceChangeListener() {
        @Override
        public void onTrayPreferenceChanged(final Collection<TrayItem> items) {
            Log.d(TAG, "read in main process: changed " + getNiceString(items));
        }
    };

    private final OnTrayPreferenceChangeListener mImportPrefsListener
            = new OnTrayPreferenceChangeListener() {
        @Override
        public void onTrayPreferenceChanged(final Collection<TrayItem> items) {
//          Log.v(TAG, "trayPrefs changed items: " + getNiceString(items));
            updateSharedPrefInfo();
        }
    };

    private final Preferences.PreferencesObserver mSharedPrefsListener
            = new Preferences.PreferencesObserver() {
        @Override
        public void onChange(Preferences preferences, String key) {
            Object value = null;
            try {
                value = mSharedPreferences.getString(key, "");
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
            try {
                value = mSharedPreferences.getInt(key, 0);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "sharedPrefs changed key: '" + key + "' value '" + value + "'");
            updateSharedPrefInfo();
        }
    };

    private void updateSharedPrefInfo() {
        final Text info = (Text) findComponentById(ResourceTable.Id_shared_pref_info);
        final String sharedPrefData = mSharedPreferences.getString(SHARED_PREF_KEY, "null");
        final String trayData = mImportPreference.getString(TRAY_PREF_KEY, "null");

        info.setText("SharedPref Data: " + sharedPrefData + "\n"
                + "Tray Data: " + trayData);
    }

    private List<String> getNiceString(final Collection<TrayItem> items) {
        return Observable.from(items)
                .map(new Func1<TrayItem, String>() {
                    @Override
                    public String call(final TrayItem trayItem) {
                        return "key: '" + trayItem.key() + "' value '" + trayItem.value() + "'";
                    }
                })
                .toList().toBlocking().first();
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_reset:
                resetAndRestart();
                break;
            case ResourceTable.Id_write_shared_pref:
                writeInSharedPref();
                break;
            case ResourceTable.Id_import_shared_pref:
                importSharedPref();
                break;
            case ResourceTable.Id_increase_multiprocess_counter:
                increaseMultiprocessCounter();
                break;
            case ResourceTable.Id_increase_multiprocess_counter_other_process:
                increaseMultiprocessCounterInOtherProcess();
                break;
            default:
                break;
        }
    }

    /**
     * resets the startup count and restarts the activity
     */
    private void resetAndRestart() {
        mAppPrefs.remove(STARTUP_COUNT);
    }

    private void writeInSharedPref() {
        final String data = "SOM3 D4T4 " + (System.currentTimeMillis() % 100000);
        mSharedPreferences
                .putString(SHARED_PREF_KEY, data)
                .flush();
    }

    private void importSharedPref() {
        final SharedPreferencesImport sharedPreferencesImport =
                new SharedPreferencesImport(this, MainAbility.SHARED_PREF_NAME,
                        MainAbility.SHARED_PREF_KEY, TRAY_PREF_KEY);
        mImportPreference.migrate(sharedPreferencesImport);
    }

    private void increaseMultiprocessCounter() {
        mMultiProcessCounter++;
        HiLog.warn(LABEL, "write in main process: counter %{public}d", mMultiProcessCounter);
        mSharedPreferences
                .putInt(ProcessServiceAbility.KEY_MULTIPROCESS_COUNTER_SERVICE_READ,
                        mMultiProcessCounter).flush();
        mAppPrefs.put(ProcessServiceAbility.KEY_MULTIPROCESS_COUNTER_SERVICE_READ,
                mMultiProcessCounter);
        int result = mSharedPreferences.getInt(ProcessServiceAbility.KEY_MULTIPROCESS_COUNTER_SERVICE_READ, 0);
        HiLog.warn(LABEL, "increase counter %{public}d, process Name %{public}s", result, getProcessName());
        // starting a service in another process to read the values there.
        ProcessServiceAbility.read(getAbility());
    }

    private void increaseMultiprocessCounterInOtherProcess() {
        // the listeners will react to those changes. At least the tray listener because the
        // listener of the shared preferences has no idea something has changed in the shared prefs
        // in another process
        ProcessServiceAbility.write(getAbility());
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSharedPreferences.unregisterObserver(mSharedPrefsListener);
        mImportPreference.unregisterOnTrayPreferenceChangeListener(mImportPrefsListener);
        mAppPrefs.unregisterOnTrayPreferenceChangeListener(mAppPrefsListener);
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        terminateAbility();
        Tray.clear(mAppPrefs);
    }
}
