package net.grandcentrix.tray.sample;

import net.grandcentrix.tray.AppPreferences;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.IntentAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.IRemoteObject;

public class IntentServiceAbility extends IntentAbility {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "Demo");


    private static final String TAG = IntentServiceAbility.class.getSimpleName();

    public static final String KEY_MULTIPROCESS_COUNTER_SERVICE_READ = "multiprocess_counter_read";

    public static final String KEY_MULTIPROCESS_COUNTER_SERVICE_WRITE
            = "multiprocess_counter_write";

    private static final String INTENT_MODE = "mode";

    private static final String INTENT_MODE_READ = "read";

    private static final String INTENT_MODE_WRITE = "write";

    private static int mCount = 0;

    private Preferences mSharedPreferences;

    private AppPreferences mTrayPreferences;

    public IntentServiceAbility(String name) {
        super(name);
    }

    @Override
    public void onStart(Intent intent) {
        HiLog.error(LABEL_LOG, "IntentServiceAbility::onStart");
        super.onStart(intent);
        mTrayPreferences = new AppPreferences(this);
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
//      String fileName = "name"; // fileName表示文件名，其取值不能为空，也不能包含路径，默认存储目录可以通过context.getPreferencesDir()获取。
        mSharedPreferences = databaseHelper.getPreferences(MainAbility.SHARED_PREF_NAME);
    }

    @Override
    public void onBackground() {
        super.onBackground();
        HiLog.info(LABEL_LOG, "IntentServiceAbility::onBackground");
    }

    @Override
    public void onStop() {
        super.onStop();
        HiLog.info(LABEL_LOG, "IntentServiceAbility::onStop");
    }

    @Override
    public void onCommand(Intent intent, boolean restart, int startId) {
    }

    @Override
    protected void onProcessIntent(Intent intent) {
        //todo
        final String mode = intent.getStringParam(INTENT_MODE);
        switch (mode) {
            case INTENT_MODE_READ: {

            }
            break;
            case INTENT_MODE_WRITE: {
                mCount++;
                mTrayPreferences.put(KEY_MULTIPROCESS_COUNTER_SERVICE_WRITE, mCount);
                mSharedPreferences
                        .putInt(KEY_MULTIPROCESS_COUNTER_SERVICE_WRITE, mCount)
                        .flush();
            }
            break;
            default:
                throw new IllegalArgumentException("unknown mode");

        }
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {
        return null;
    }

    @Override
    public void onDisconnect(Intent intent) {
    }

    /**
     * reads data from shared preferences and tray and prints the values. This happens in a
     * different process
     */
    public static void read(Ability ability) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.ohos.hw")
                .withAbilityName("com.ohos.hw.IntentServiceAbility")
                .build();
        intent.setParam(INTENT_MODE,INTENT_MODE_READ);
        intent.setOperation(operation);
        ability.startAbility(intent);
    }

    /**
     * increases the number in shared preferences and tray. This happens in a different process
     */
    public static void write(Ability Ability) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.ohos.hw")
                .withAbilityName("com.ohos.hw.IntentServiceAbility")
                .build();
        intent.setOperation(operation);
        Ability.startAbility(intent);
    }
}