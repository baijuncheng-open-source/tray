package net.grandcentrix.tray.sample;

import net.grandcentrix.tray.sample.slice.TestAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    public static final String STARTUP_COUNT = "startup_count";

    public static final String SHARED_PREF_NAME = "shared_pref";

    public static final String SHARED_PREF_KEY = "shared_pref_key";

    public static final String TRAY_PREF_KEY = "importedData";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TestAbilitySlice.class.getName());
//        super.setMainRoute(MainAbilitySlice.class.getName());
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        terminateAbility();
    }
}
